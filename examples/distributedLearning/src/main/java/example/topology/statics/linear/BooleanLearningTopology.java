package example.topology.statics.linear;

import java.io.IOException;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.BoltDeclarer;
import backtype.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISyncOp;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import eu.ferari.core.sync.aggregation.Averaging;
import eu.ferari.core.sync.coord.StaticCoordSyncOp;
import eu.ferari.core.sync.local.StaticLocalSyncOp;
import eu.ferari.learning.ComEffLearner;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import example.functions.BinaryInputStream;
import example.functions.BooleanHypothesis;
import example.functions.Hypothesis;
import example.functions.InputStream;
import example.spout.SyntheticDataSpout;

/**
 * an example of a static learning topology using Linear model
 * @author Rania
 *
 */
public class BooleanLearningTopology {

    private static final String DataSpout = "dataSpout";
 
    private static final String LearnerBoltName = "learnerBolt";
    
    private static final String coordinatorBoltName = "coordinatorBolt";

    public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
            InvalidTopologyException, IOException {

        // Config settings
        Config conf = new Config();
        conf.setDebug(false);

       // conf.put("INFILE", countDistinctConstants.INFILENAME);

        // Build the topology
        TopologyBuilder builder = new TopologyBuilder();

        Hypothesis <Boolean> hypothesis = new BooleanHypothesis(new int [] {7}, new int [] {}, 10);
        InputStream inputStream = new BinaryInputStream(hypothesis, -1);
        SyntheticDataSpout dataSpout = new SyntheticDataSpout(inputStream);

        builder.setSpout(DataSpout, dataSpout,1);
        
        ILocalSyncOp <Integer, Double> localSyncOperator = new StaticLocalSyncOp<Integer, Double>(100);
        Averaging<Integer, Double> meanSync = new Averaging<Integer, Double>();
        ICoordTaskOperator <Integer, Double> coordinatorSyncOperator = new StaticCoordSyncOp<Integer, Double>(meanSync);
        
        ISyncOp <Integer, Double>  syncOper = new ISyncOp<Integer, Double> (localSyncOperator, coordinatorSyncOperator );
        ComEffLearner<Integer, Double> comEffLearner = new ComEffLearner<Integer, Double> (LossFunctionType.HingeLoss, ModelType.LinearClassification);
        ComEffNode <Integer, Double> commEffNode = new ComEffNode<Integer, Double>(comEffLearner, syncOper);
        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(commEffNode), 13);
        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
        ComEffCoordinator <Integer, Double> comEffCoordinator = new ComEffCoordinator<Integer, Double>(syncOper);
       
        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
        
        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        learnerBolt = learnerBolt.allGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME);

      
        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("test", conf, builder.createTopology());
        Thread.sleep(10000000);
        cluster.shutdown();
        
    }
}


