package example.topology.statics.kernel;

import java.io.IOException;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.BoltDeclarer;
import backtype.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.BulkDirectGrouping;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISyncOp;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import eu.ferari.core.sync.coord.StaticCoordSyncOp;
import eu.ferari.core.sync.local.StaticLocalSyncOp;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.ComEffLearner;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import eu.ferari.learning.sync.KernelAggregation;
import example.functions.Hypothesis;
import example.functions.InputStream;
import example.functions.PolynomialHypothesis;
import example.functions.RealValuedInputStream;
import example.spout.SyntheticDataSpout;


/**
 * an example of a static learning topology
 * @author Rania
 *
 */
public class KernelPolynomialTopology {

    private static final String DataSpout = "dataSpout";
 
    private static final String LearnerBoltName = "learnerBolt";
    
    private static final String coordinatorBoltName = "coordinatorBolt";

    public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
            InvalidTopologyException, IOException {

        // Config settings
        Config conf = new Config();
        conf.setDebug(false);

       // conf.put("INFILE", countDistinctConstants.INFILENAME);

        // Build the topology
        TopologyBuilder builder = new TopologyBuilder();
        int[][] polynom = initPolynomial();
        Hypothesis <Double> hypothesis = new PolynomialHypothesis(new double[] {0.5, 1.2, 1}, polynom);
        //RegressionFunction hypothesis = HypothesisGenerator.generateRegressionHypothesis(Parameters.DATA_LENGTH);
        //System.out.println("Initial hypothesis: " + Arrays.toString(hypothesis.getCoeffs()));
        InputStream inputStream = new RealValuedInputStream(hypothesis, -1);
        SyntheticDataSpout dataSpout = new SyntheticDataSpout(inputStream, 0);

        builder.setSpout(DataSpout, dataSpout, 1);
        
        ILocalSyncOp <RealValuedVector, Double> localSyncOperator = new StaticLocalSyncOp<RealValuedVector, Double>(100);
        KernelAggregation kernelSync = new KernelAggregation();
        ICoordTaskOperator <RealValuedVector, Double> coordinatorSyncOperator = new StaticCoordSyncOp<RealValuedVector, Double>(kernelSync);
        
        ISyncOp<RealValuedVector, Double>syncOper = new ISyncOp<RealValuedVector, Double>(localSyncOperator, coordinatorSyncOperator );
        
        ComEffLearner<RealValuedVector, Double> commEffLearner = new ComEffLearner<RealValuedVector, Double> (LossFunctionType.SquareError, ModelType.KernelRegression);
        ComEffNode <RealValuedVector, Double> comEffNode = new ComEffNode <RealValuedVector, Double> (commEffLearner, syncOper);
        
        comEffNode.setLogAnnotation("static");
        commEffLearner.setLogAnnotation("static");
        
        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(comEffNode), 13);
        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
        ComEffCoordinator <RealValuedVector, Double> comEffCoordinator = new ComEffCoordinator<RealValuedVector, Double>(syncOper);
       
        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
        
        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME, new BulkDirectGrouping());

        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("test", conf, builder.createTopology());
        Thread.sleep(10000000);
        cluster.shutdown();
        
    }

	private static int[][] initPolynomial() {
		 int [][] polynomial = new int[3][];
	        polynomial[0] = new int[2];
	        polynomial[1] = new int[2];
	        polynomial[2] = new int[1];
	        polynomial[0][0] = 0;
	        polynomial[0][1] = 2;
	        polynomial[1][0] = 1;
	        polynomial[1][1] = 2;
	        polynomial[2][0] = 1;
	        // x0*x2+x1*x2+x1
	        return polynomial;
	}
}

