package example.topology.statics.linear;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.BoltDeclarer;
import backtype.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.BulkDirectGrouping;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISyncOp;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import eu.ferari.core.sync.aggregation.Averaging;
import eu.ferari.core.sync.coord.StaticCoordSyncOp;
import eu.ferari.core.sync.local.StaticLocalSyncOp;
import eu.ferari.learning.ComEffLearner;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.model.tmp.LearningParamsMap.LearningParameter;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import example.spout.StockDataSpout;


	/**
	 * learning stock price using a linear model
	 * @author Rania
	 *
	 */
	public class StockLearningTopologyLinear {

	    private static final String DataSpout = "dataSpout";
	 
	    private static final String LearnerBoltName = "learnerBolt";
	    
	    private static final String coordinatorBoltName = "coordinatorBolt";

	    public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
	            InvalidTopologyException, IOException {

	        // Config settings
	        Config conf = new Config();
	        conf.setDebug(false);

	       // conf.put("INFILE", countDistinctConstants.INFILENAME);

	        // Build the topology
	        TopologyBuilder builder = new TopologyBuilder();
	        StockDataSpout dataSpout = new StockDataSpout("SP100_2007_2011.fdc", -1);

	        builder.setSpout(DataSpout, dataSpout, 1);
	        
	        ILocalSyncOp <Integer, Double> localSyncOperator = new StaticLocalSyncOp<Integer, Double>(100);
	        Averaging <Integer, Double> meanSync = new Averaging<Integer, Double>();
	        
	        ICoordTaskOperator <Integer, Double> coordinatorSyncOperator = new StaticCoordSyncOp<Integer, Double>(meanSync);
	        ISyncOp<Integer, Double>syncOper = new ISyncOp<Integer, Double>(localSyncOperator, coordinatorSyncOperator);
	        LearningParamsMap paramsMap = initLearningParams();
	        ComEffLearner<Integer, Double> commEffLearner = new ComEffLearner<Integer, Double> (LossFunctionType.EpsilonInsensitive, ModelType.KernelRegression, paramsMap);
	        ComEffNode <Integer, Double> comEffNode = new ComEffNode <Integer, Double> (commEffLearner, syncOper);
	        
	        comEffNode.setLogAnnotation("static");
	        commEffLearner.setLogAnnotation("static");
	        
	        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(comEffNode), 13);
	        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
	        ComEffCoordinator <Integer, Double> comEffCoordinator = new ComEffCoordinator<Integer, Double>(syncOper);
	       
	        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
	        
	        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

	        learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME, new BulkDirectGrouping());

	        LocalCluster cluster = new LocalCluster();

	        cluster.submitTopology("test", conf, builder.createTopology());
	        Thread.sleep(10000000);
	        cluster.shutdown();
	        
	    }

		private static LearningParamsMap initLearningParams() {
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put(String.valueOf(LearningParameter.LinearLearningRate), "0.1");
			paramsMap.put(String.valueOf(LearningParameter.GaussianWidth), "814088");
			paramsMap.put(String.valueOf(LearningParameter.KernelLearningRate), "0.32");
			paramsMap.put(String.valueOf(LearningParameter.PolynomialKernelDegree), "2");
			// the regularization constant of the kernel stochastic update rule 
			paramsMap.put(String.valueOf(LearningParameter.Regularizer), "0.00000005");
			// the polynomial function constant
			paramsMap.put(String.valueOf(LearningParameter.c), "0.0002");
			// the kernel function to be used in the kernel model, exmaple linear, gaussian, polynomial
			paramsMap.put(String.valueOf(LearningParameter.KernelFunction), "Gaussian");
			return new LearningParamsMap(paramsMap);
		}
	}
