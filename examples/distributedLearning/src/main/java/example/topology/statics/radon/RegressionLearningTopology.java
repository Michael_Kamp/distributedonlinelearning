package example.topology.statics.radon;

import java.io.IOException;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.BoltDeclarer;
import backtype.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.BulkDirectGrouping;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISyncOp;
import eu.ferari.core.state.ComEffCoordinator;
import eu.ferari.core.state.ComEffNode;
import eu.ferari.core.sync.coord.StaticCoordSyncOp;
import eu.ferari.core.sync.local.StaticLocalSyncOp;
import eu.ferari.learning.ComEffLearner;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import eu.ferari.learning.sync.RadonAggregation;
import example.functions.InputStream;
import example.functions.RealValuedInputStream;
import example.functions.RegressionFunction;
import example.spout.SyntheticDataSpout;


/**
 * an example of a static learning topology using the iterated Radon point algorithm
 * @author Rania
 *
 */
public class RegressionLearningTopology {

    private static final String DataSpout = "dataSpout";
 
    private static final String LearnerBoltName = "learnerBolt";
    
    private static final String coordinatorBoltName = "coordinatorBolt";

    public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
            InvalidTopologyException, IOException {

        // Config settings
        Config conf = new Config();
        conf.setDebug(false);

       // conf.put("INFILE", countDistinctConstants.INFILENAME);

        // Build the topology
        TopologyBuilder builder = new TopologyBuilder();

        RegressionFunction hypothesis = new RegressionFunction(new double [] {7.9, 6.9, 4.3, 0.6, 7.8, 8.8, 0.9, 1.1, 1.8, 1.8});
        //RegressionFunction hypothesis = HypothesisGenerator.generateRegressionHypothesis(Parameters.DATA_LENGTH);
        //System.out.println("Initial hypothesis: " + Arrays.toString(hypothesis.getCoeffs()));
        InputStream inputStream = new RealValuedInputStream(hypothesis, -1);
        SyntheticDataSpout dataSpout = new SyntheticDataSpout(inputStream, 0);

        builder.setSpout(DataSpout, dataSpout, 1);
        
        ILocalSyncOp <Integer, Double> localSyncOperator = new StaticLocalSyncOp<Integer, Double>(100);
        RadonAggregation <Integer, Double> averagingFunction = new RadonAggregation<Integer, Double>();
        ICoordTaskOperator <Integer, Double> coordinatorSyncOperator = new StaticCoordSyncOp<Integer, Double>(averagingFunction);
        
        ISyncOp<Integer, Double> syncOper = new ISyncOp<Integer, Double>(localSyncOperator, coordinatorSyncOperator );
        
        ComEffLearner<Integer, Double> commEffLearner = new ComEffLearner<Integer, Double> (LossFunctionType.SquareError, ModelType.LinearRegression);
        ComEffNode <Integer, Double> comEffNode = new ComEffNode <Integer, Double> (commEffLearner, syncOper);
        
        comEffNode.setLogAnnotation("static");
        commEffLearner.setLogAnnotation("static");
        
        BoltDeclarer learnerBolt = builder.setBolt(LearnerBoltName, new LocalBolt(comEffNode), 13);
        learnerBolt = learnerBolt.shuffleGrouping(DataSpout);
        ComEffCoordinator <Integer, Double> comEffCoordinator = new ComEffCoordinator<Integer, Double>(syncOper);
       
        BoltDeclarer coordinatorBolt = builder.setBolt(coordinatorBoltName, new CoordinatorBolt(comEffCoordinator), 1);
        
        coordinatorBolt = coordinatorBolt.allGrouping(LearnerBoltName, StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        //learnerBolt = learnerBolt.allGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME);
        //learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, BulkDirectGrouping.POLL_LOCAL_STREAM, new BulkDirectGrouping());
        learnerBolt = learnerBolt.customGrouping(coordinatorBoltName, StormSendToLocal.TO_LOCAL_STREAM_NAME, new BulkDirectGrouping());

        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("test", conf, builder.createTopology());
        Thread.sleep(10000000);
        cluster.shutdown();
        
    }
}


