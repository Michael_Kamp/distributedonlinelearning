package example.functions;

import java.util.Random;

/**
 * represents a Gaussian distribution function
 */
public class DiscreteUniformPdf extends ProbabilityDistibutionFunction<Integer>{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1821212740429543741L;
	
	private Random random = new Random();

	/**
	 * the upper limit of the discrete distribution
	 */
	private int n = Integer.MAX_VALUE;
	public DiscreteUniformPdf() {
	}
	public DiscreteUniformPdf(int n) {
		this.n = n;
	}
	


	@Override
	public Integer[] generateInstance() {
		Integer instance[] = new Integer[dimension];
		for(int i =0; i<dimension; i++) {
			instance[i] = random.nextInt(n);
			random.nextInt();
		}
		return instance;
	}

}
