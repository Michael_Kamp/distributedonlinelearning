package example.functions;

import backtype.storm.tuple.Values;

/**
 * An example data generator spout for the learning algorithm, the learning model is classification
 * @author Rania
 *
 */
public class BinaryInputStream extends InputStream{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -7277760897264706307L;


	public BinaryInputStream (Hypothesis<?> hypothesis, int count) {
		super(hypothesis, count);
	}
	
	
	public Values nextExample() {
		if (count > 0 || count < 0) {
			Boolean [] instance = generateRandomTuple();
			Values values = new Values();
	        values.add(count);
	        values.add(instance);
	        if(count > 0) {
	        	--count;
	        }
			return values;
		}
		return null;
	}
	
	
	private Boolean[] generateRandomTuple() {
		int hypothesisDim = ((Hypothesis <Double>)(function)).getHypothesisLength();
		Boolean[] instance = new Boolean [hypothesisDim+1];
		for(int i = 0; i < hypothesisDim; i++) {
			instance[i] = random.nextBoolean();
		}
		instance[hypothesisDim] = ((Hypothesis <Boolean>)function).evaluateInstance(instance);
		return instance;
	}

}