package example.functions;

import java.util.Random;

/**
 * represents a uniform distribution function
 */
public class UniformPDF extends ProbabilityDistibutionFunction<Double>{

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1821212740429543741L;
	
	private Random random = new Random();

	@Override
	public Double[] generateInstance() {
		Double instance[] = new Double[dimension];
		for(int i =0; i<dimension; i++) {
			instance[i] = random.nextDouble();
		}
		return instance;
	}

}
