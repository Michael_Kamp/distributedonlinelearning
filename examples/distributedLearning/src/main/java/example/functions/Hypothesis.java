package example.functions;

/**
 * Represents a hypothesis 
 * @author Rania
 *
 * @param <T>
 */
public abstract class Hypothesis <T> implements Function{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5509930849682610894L;
	
	public abstract T evaluateInstance(T[] instance);
	
	public abstract int getHypothesisLength();

	

}
