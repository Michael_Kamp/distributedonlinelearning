package example.functions;

import java.util.Random;


public class HypothesisGenerator {
	
	private static Random rnd = new Random();
	
	public static RegressionFunction generateRegressionHypothesis(int dim) {
		double [] rndCoefficient = new double[dim];
		for(int i = 0 ; i<dim; i++) {
			double coeff = rnd.nextInt(10) + rnd.nextDouble();
			coeff = Math.floor(coeff * 10) / 10;

			rndCoefficient[i] = coeff;
		}
		RegressionFunction hypothesis = new RegressionFunction(rndCoefficient);
		return hypothesis;
	}
}