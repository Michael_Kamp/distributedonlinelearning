package example.functions;

import backtype.storm.tuple.Values;
/**
 * a probability distribution input stream, invokes generateInstance() of a given pdf to generate the next sample. 
 * @author Rania
 * Type: Double in case of real valued distribution,  integer in case of discrete
 */
public class ProbDistInputStream <Type> extends InputStream{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6512234226137824695L;

	public ProbDistInputStream(ProbabilityDistibutionFunction<?> pdf) {
		this.function = pdf;
	}
	/**
	 * 
	 * @param pdf
	 * @param numInstances the number of instances to be generated
	 */
	public ProbDistInputStream(ProbabilityDistibutionFunction<?> pdf, int numInstances) {
		super(pdf, numInstances);
	}

	public Values nextExample() {
		if (count > 0 || count < 0) {
			Type [] instance = ((ProbabilityDistibutionFunction<Type>)function).generateInstance();
			Values values = new Values();
	        values.add(count);
	        values.add(instance);
			--count;
			return values;
		}
		return null;
	}
	

}
