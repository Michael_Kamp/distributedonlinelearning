package example.functions;

import java.io.Serializable;

/**
 * This class is a function interface, to be implemented by a hypothesis or probability distribution function. 
 * @author Rania
 *
 */
public interface Function <T> extends Serializable{


}
