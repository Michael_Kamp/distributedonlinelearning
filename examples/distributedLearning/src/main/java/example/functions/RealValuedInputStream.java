package example.functions;

import backtype.storm.tuple.Values;

/**
 * An example data generator spout for the learning algorithm, the learning model is classification
 * @author Rania
 *
 */
public class RealValuedInputStream extends InputStream{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 2948337383455771363L;
	private int conceptDrift = 0;
	private double noiseStd = 0;


	public RealValuedInputStream (Hypothesis <Double> hypothesis, int count) {
		super(hypothesis, count);
	}
	

	public RealValuedInputStream (Hypothesis <Double> hypothesis, int count, int conceptDrift, double noiseStd) {
		super(hypothesis, count);
		this.conceptDrift  = conceptDrift;
		this.noiseStd  = noiseStd;
	}
	
	
	public Values nextExample() {
		if (count > 0 || count < 0) {
			Double [] instance = generateRandomTuple();
			Values values = new Values();
	        values.add(count);
	        values.add(instance);
			--count;
			driftConcept();
			return values;
		}
		return null;
	}
	
	
	private void driftConcept() {
		if(conceptDrift < 1)
			return;
		instancesCount++;
		if(instancesCount == conceptDrift) {
			instancesCount = 0;
			System.out.println("Concept drift occurred");
			function = HypothesisGenerator.generateRegressionHypothesis(10);
		}
		
	}


	private Double[] generateRandomTuple() {
		int hypothesisDim = ((Hypothesis <Double>)(function)).getHypothesisLength();
		Double [] instance = new Double [hypothesisDim+1];
		//instance[0] = 1.0;
		for(int i = 0; i < hypothesisDim; i++) {
			instance[i] = random.nextDouble();// + random.nextInt(10);
		}
		// the last index is for the output value
		instance[hypothesisDim] = ((Hypothesis <Double>)function).evaluateInstance(instance) + noiseStd * random.nextGaussian();
		return instance;
	}

}