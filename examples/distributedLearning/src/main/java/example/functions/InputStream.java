package example.functions;

import java.io.Serializable;
import java.util.Random;

import backtype.storm.tuple.Values;

/**
 * An example data generator spout for the learning algorithm, the learning model is classification
 * @author Rania
 *
 */
public abstract class InputStream implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4549999045442462256L;
	// the number of tuples that we are generating
	protected int count = -1;
	protected final Random random = new Random();
	protected Function<?> function;
	
	protected int instancesCount = 0 ;
	
	public InputStream(){
		
	}
	public InputStream (Function<?> hypothesis, int count) {
		this.function = hypothesis;
		this.count = count;
	}

	
	public abstract Values nextExample() ;
}