package tuning;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.base.ILearningModel;
import eu.ferari.learning.model.tmp.LearningModelFactory;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;


/**
 * the learner node
 * @author Rania
 *
 */
public class ComEffLearner <Index, Value>  {

	
	private ILearningModel <Index, Value> model;
	private double accumLossValue = 0;
	private int instancesCount = 0;
	private int correctPredCount = 0;
	private static final long serialVersionUID = 3563792621962843627L;
	
	
	
	public ComEffLearner(LossFunctionType lossType, ModelType modelType) {
		model = LearningModelFactory.<Index, Value>getModel(modelType, lossType);
	}

	public void setLogAnnotation(String stAnnotation){ //stupid hack to distinguish logs of different topologies
		model.setLogAnnotation(stAnnotation);
	}

	/**
	 * process data instance
	 * @param instance
	 */
	public void processInput(Object [] instance){
		LearningInstance dataObject = new LearningInstance(instance);
		model.predictInstance(dataObject);
		model.updateModel(dataObject);
		instancesCount++;
		accumLossValue+= dataObject.getLossValue();
		//System.out.println("current loss value: " + dataObject.getLossValue());
		double avgLossValue = accumLossValue/instancesCount;
		if(dataObject.isPredCorrect()) 
			correctPredCount++;
		System.out.println("avg loss value so far: " + avgLossValue);
		System.out.println("accuracy so far: " + (float)correctPredCount/instancesCount);
	}

	public IVector <Index, Value>  getLocalVariable() {
		return model.getWeights();
	}


	public void setLocalVariable(IVector <Index, Value>  v) {
			model.setSyncModel(v);		
	}

}