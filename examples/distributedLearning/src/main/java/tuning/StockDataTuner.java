package tuning;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import backtype.storm.tuple.Values;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.ComEffLearner;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;

/**
 * The class is responsible for fitting learning parameters of a learning
 * algorithm that best fit the learning task at hand. Basically we will take
 * several values for the learning rate, regularizer and try fit find the best
 * fit emperically.
 * 
 * @author Rania
 *
 */
public class StockDataTuner {

	public static void main(String args[]) {
		StockDataTuner tuning = new StockDataTuner("SP100_2007_2011.fdc");
		tuning.readData(200);

		tuning.tuneParameters("SP100_2007_2011_test.fdc");

	}

	private String filename;

	private long count;

	private int numStocks;
	private final static int maxInterval = 200;
	private int[] averageIntervals = new int[] { 5, 11, 50, maxInterval };
	// we need to keep track of the last (maxInterval) values for all features
	// for performing the average
	private double[][] lastValues;
	// the index of the stock value that we would like to predict
	private int targetIndex = -1;

	private List<Double[]> featuresArray = new ArrayList<>();

	Double[] prevFeatures;
	private double sigma;
	private ComEffLearner<RealValuedVector, Double> learner;

	public StockDataTuner(String filename) {
		this.filename = filename;
		learner = new ComEffLearner<RealValuedVector, Double>(
				LossFunctionType.SquareError, ModelType.KernelRegression);
	}

	public StockDataTuner(String filename, int targetIndex) {
		this(filename);
		this.targetIndex = targetIndex;
	}

	public void readData(int numLines) {
		BufferedReader br = null;
		File inFile = new File(filename);
		Double[] instance = null;
		try {
			br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine();
			numStocks = line.split("\t").length - 1; // the first column is for
														// the date
			if (targetIndex < 0) {
				targetIndex = numStocks - 1;
			}
			lastValues = new double[numStocks][maxInterval]; 			
																
			while ((line = br.readLine()) != null) {
				instance = getInstance(line);
				if (prevFeatures != null) {
					prevFeatures[prevFeatures.length - 1] = instance[targetIndex];
					prevFeatures = formFeaturesVector(instance);
					featuresArray.add(prevFeatures);
				}
				++count;
				prevFeatures = formFeaturesVector(instance);
				if (count == numLines) {
					sigma = computeMeanDistance();
					break;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

	}

	public void tuneParameters(String testFilename) {
		Map <String, Double> parametersMap = new HashMap<String, Double>();
		double learningRate = 0;
		double step = 0.00005;
		while (learningRate < 0.001) {
			learningRate += step;
			parametersMap.put("learningRate", learningRate);
			double regularizer = 0; 
			while (regularizer < 0.0005) {
				regularizer += step;
				System.out.println("*************************** tuning with the following parameters: \n learning rate, regularizer= " + learningRate + ", " + regularizer);
				parametersMap.put("regularizer", regularizer);
				learner.setLocalVariable(null); //reset the model
				learner.setParameters(parametersMap);
			prevFeatures = null;
			count = 0;
			Double[] instance = null;
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(testFilename));
				br.readLine(); //skip the headers					
				lastValues = new double[numStocks][maxInterval]; // save last 200 entries for each stock company
				String line;
				while ((line = br.readLine()) != null) {
					instance = getInstance(line);
					++count;
			 		Values values = null;
			 		if(count >= maxInterval) {
			 			if(prevFeatures != null) {
			 				prevFeatures[prevFeatures.length-1] = instance[targetIndex];
			 				values = new Values();
			 			    values.add(1);
			 			    values.add(prevFeatures);
			 			    learner.processInput(prevFeatures);
			 			}
			 			prevFeatures = formFeaturesVector(instance);
			 		}

			}} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null)
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
		}
	}

	// compute the euclidean distance between each pair of features vectors and
	// then divide the number of
	// distances calculated to obtain a mean value, this will be our sigma in
	// the gaussian model
	private double computeMeanDistance() {
		double totalDistance = 0;
		for (int i = 0; i < featuresArray.size(); i++) {
			for (int j = i + 1; j < featuresArray.size(); j++) {
				double currentDistance = computeDistance(featuresArray.get(i),
						featuresArray.get(j));
				totalDistance += currentDistance;
			}
		}
		int numDistances = (featuresArray.size() * (featuresArray.size() - 1)) / 2;
		double meanDistance = totalDistance / numDistances;
		System.out.println("********mean distance: " + meanDistance);
		return meanDistance;
	}

	public double computeDistance(Double[] point1, Double[] point2) {
		double dist = 0;
		for (int i = 0; i < point1.length; i++) {
			dist += Math.pow(point1[i] - point2[i], 2);
		}
		return Math.sqrt(dist);
	}

	private Double[] formFeaturesVector(Double[] instance) {
		Double features[] = new Double[numStocks + numStocks
				* averageIntervals.length + 1];

		System.arraycopy(instance, 0, features, 0, numStocks);
		for (int i = 0; i < averageIntervals.length; i++) {
			for (int k = 0; k < numStocks; k++) {
				Double average = getAvgLast(averageIntervals[i], k);
				features[instance.length + i * numStocks + k] = average;
			}
		}
		features[features.length - 1] = instance[targetIndex];
		if (Arrays.asList(features).contains(null)) {
			Arrays.asList(features).indexOf(null);
		}
		return features;
	}

	private double getAvgLast(int lastNumDays, int stockumber) {
		double average;
		double[] subArray;
		if (lastNumDays < maxInterval) {
			subArray = Arrays.copyOfRange(lastValues[stockumber],
					lastValues.length - lastNumDays,
					lastValues[stockumber].length);
		} else {
			subArray = lastValues[stockumber];
		}

		average = Arrays.stream(subArray).map(i -> i).average().getAsDouble();
		return average;
	}

	private Double[] getInstance(String row) {
		String[] rawInstance = row.split("\t");
		Double[] instance = new Double[rawInstance.length - 1];
		for (int i = 1; i < rawInstance.length; i++) {
			double value = Double.parseDouble(rawInstance[i]);
			instance[i - 1] = value;
			lastValues[i - 1][(int) count % maxInterval] = value;
		}
		return instance;
	}

}
