var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);

io.sockets.on('connection', function(socket){
    console.log("Someone has connected");
    socket.on("learnerEvents", function(message, cb) {
    	console.log(message);
		receiveEvent(message);
	cb('');
    });

    socket.on('disconnect', function() {
    });
});


server.listen(3001, function () {
    'use strict';
});
