package eu.ferari.protonAdapter;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import com.ibm.hrl.proton.adapters.interfaces.AdapterException;

import json.java.JSONArray;
import json.java.JSONObject;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

public class FileReaderSpout extends BaseRichSpout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String path=null;
	private SpoutOutputCollector _collector;
	private BufferedReader reader;
	private boolean done=false;
	/**
	 * Creates a file reader spout from the configuration stored in a JSON Array.
	 * @param array Must contain an element with "name" = "filename"  and "value" = PATH_TO_FILE
	 */
	public FileReaderSpout(JSONArray array){
		//TODO add polling information!
		JSONObject jsonObj;
		String name;
		for(Object obj : array){
			jsonObj = (JSONObject) obj;
			name = (String) jsonObj.get("name");
			if(name == null || (!name.equals("filename"))) continue;
			path=(String) jsonObj.get("value");
			return;//found information!
		}
		throw new NoFilenameParameterException();
		
	}

	@Override
	public void nextTuple() {
		if(done) return;
		String line="";
		try {
			line = reader.readLine();
		}catch (IOException e) {
			// TODO Another exception.
			e.printStackTrace();
		}
		if(line!=null) _collector.emit(new Values(line));
		else done =true;
	}

	@Override
	public void open(Map arg0, TopologyContext arg1, SpoutOutputCollector arg2) {
		_collector=arg2;
		try {
			reader= new BufferedReader(new FileReader(new File(path)));
		} catch (FileNotFoundException e) {
			// TODO Make real exception
			e.printStackTrace();
		}

	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("InputLine"));

	}

}
