package eu.ferari.protonAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import json.java.JSONArray;
import json.java.JSONObject;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import clojure.lang.Obj;

import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.ibm.hrl.proton.utilities.containers.Pair;

public class CsvFormatterBolt extends BaseRichBolt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private OutputCollector _collector;
	private String _eventName;
	private CSVTextFormatter formatter;
	public final static String _DEFAULT_DELIMITER = ",";
	public final static String _DEFAULT_DATE_FORMAT =  "dd/MM/yyyy-HH:mm:ss";
	double prevTimestamp =0;
	//TODO use DATE string!!
	public CsvFormatterBolt (JSONArray producerProperties, JSONArray events){
		JSONObject jsonObj;
		JSONArray fields;
		String name;
		String dateFormatString = _DEFAULT_DATE_FORMAT;
		String delimiter = _DEFAULT_DELIMITER;
		Pair<Attribute.Type,String> attributeDescription;
		Map<String,Pair<Attribute.Type, String>>eventTypes=null;
		ArrayList<Attribute> attributeDescriptions=null;
		
		
		String attributeName;
		for(Object obj : producerProperties){
			jsonObj=(JSONObject) obj;
			name=(String) jsonObj.get("name");
			switch(name){
			case "formatter":
				//TODO parse from CSV first and check if all are consumed.
				if(!((String)jsonObj.get("value")).toLowerCase().equals("csv")) continue;
				//If there are multiple csv sections we take the last!
				attributeDescriptions = new ArrayList<Attribute>();
				_eventName=(String) jsonObj.get("eventName");
				eventTypes=getEventTypes(_eventName, events);
				fields = (JSONArray) jsonObj.get("attributes");
				for(Object obj2: fields){
					attributeName=((String) obj2);
					attributeDescription= eventTypes.remove(attributeName);
					//TODO EXCEPTION for null
					attributeDescriptions.add(new Attribute(attributeName,
										attributeDescription.getFirstValue(),
										attributeDescription.getSecondValue()));
				}
				for(String attrName : eventTypes.keySet()){
					attributeDescription=eventTypes.get(attrName);
					attributeDescriptions.add(new Attribute(attrName, attributeDescription.getFirstValue(),attributeDescription.getSecondValue()));
				}
				break;
			
			case "dateFormat":
				dateFormatString = (String) jsonObj.get("value");
				break;
			
			case "delimiter":
				delimiter= (String) jsonObj.get("value");
				break;
			
			default:
				continue;
			}
			
		}
		try{
			formatter = new CSVTextFormatter(attributeDescriptions, dateFormatString, delimiter);
		}catch(NullPointerException e){
			throw new MissingCSVSectionException();
		}
	}
	private Map<String, Pair<Attribute.Type, String>> getEventTypes(String eventName,
			JSONArray events) {
		Map<String, Pair<Attribute.Type, String>> map = new HashMap<String, Pair<Attribute.Type,String>>();
		JSONArray attributes;
		String parsedName;
		JSONObject eventDescription, eventAttribute;
		for(Object obj : events){
			eventDescription = (JSONObject) obj;
			parsedName= (String) eventDescription.get("name");
			if(parsedName == null || !parsedName.equals(eventName)) continue;
			//The correct event is in eventDescription!!
			String attributeName, defaultValue;
			Attribute.Type attributeType;
			attributes = (JSONArray) eventDescription.get("attributes");
			for(Object obj2 : attributes){
				eventAttribute=(JSONObject) obj2;
				attributeName=(String)eventAttribute.get("name");
				defaultValue=(String)eventAttribute.get("defaultValue");
				attributeType=Attribute.Type.valueOf((String)eventAttribute.get("type"));
				map.put(attributeName, new Pair<Attribute.Type, String>(attributeType,defaultValue));
			}
			break;//There should be only one event.
			
		}
		return map;
	}
	@Override
	public void execute(Tuple tuple) {
		Map<String,Object> event = formatter.parseText(tuple.getString(0));
		if(event.get("OccurrenceTime")==null) System.out.println("Could not get timestamp from Event");
		double timestamp=(Double) event.get("OccurrenceTime");
		
		double delayMillis = prevTimestamp > 0 ? timestamp - prevTimestamp : 0;
		prevTimestamp = delayMillis >0 ? timestamp : 0;
		try {
			Thread.sleep((long) delayMillis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		event.put("OccurrenceTime", 1.0*timestamp);
		_collector.emit(new Values(_eventName,event));
		_collector.ack(tuple);
		
	}

	@Override
	public void prepare(Map arg0, TopologyContext arg1, OutputCollector arg2) {
		_collector=arg2;

	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("Name",STORMMetadataFacade.ATTRIBUTES_FIELD));

	}

}
