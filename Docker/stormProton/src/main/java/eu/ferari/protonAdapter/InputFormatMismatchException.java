package eu.ferari.protonAdapter;

import eu.ferari.protonAdapter.Attribute.Type;


public class InputFormatMismatchException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String input;
	private Type type;

	public InputFormatMismatchException(String input, Type type){
		super();
		this.input=input;
		this.type=type;
	}
	
	
	@Override
	public void printStackTrace() {
		System.err.printf("Could not parse %s as %s", input, type.toString());
		super.printStackTrace();
	}

}
