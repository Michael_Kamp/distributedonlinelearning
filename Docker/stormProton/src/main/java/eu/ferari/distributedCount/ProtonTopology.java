/*******************************************************************************
 * Copyright 2015 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package eu.ferari.distributedCount;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.StormTopology;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import com.ibm.hrl.proton.agents.EPAManagerBolt;
import com.ibm.hrl.proton.context.ContextBolt;
import com.ibm.hrl.proton.expression.facade.EEPException;
import com.ibm.hrl.proton.expression.facade.EepFacade;
import com.ibm.hrl.proton.metadata.parser.ParsingException;
import com.ibm.hrl.proton.routing.RoutingBolt;
import com.ibm.hrl.proton.routing.STORMMetadataFacade;
import com.ibm.hrl.proton.server.timerService.TimerServiceFacade;
import com.ibm.hrl.proton.server.workManager.WorkManagerFacade;
import com.ibm.hrl.proton.utilities.facadesManager.FacadesManager;
import eu.ferari.protonAdapter.*;
import json.java.JSON;
import json.java.JSONArray;
import json.java.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;



public class ProtonTopology {
	
	private static final String INPUT_NAME="input";
	private static final String FORMATTING_BOLT_NAME = "formattingBolt";
	private static final String ROUTING_BOLT_NAME ="routingBolt";
	private static final String CONTEXT_BOLT_NAME = "contextBolt";
	private static final String EPA_MANAGER_BOLT_NAME = "epaManagerBolt";
	private static final String OUTPUT_BOLT_NAME="outputBolt";
	
	public static final String CALL_DATA_READ = "CallData";	
	//private static final String ATTR_PHONE_NUMBER = "phoneNumber";
	//private static final String CALL_DURATION = "callDuration";
	//private static final String NODE_ID = "nodeID";
	//private static final String ATTR_TIMESTAMP = "timestamp";
	//private static String PATH="events.txt";

	 public static void main(String[] args) throws Exception {
		 String topologyName = "test";   
		 
		 	
		 	String jsonFileName = args[0];// "/home/proton/workspace/DistributedCount/simulatedDistributedCounter.json";

		 	
		 	
		    TopologyBuilder builder = CreateProtonTopology(jsonFileName, topologyName);
		    

		    Config conf = new Config();
		    conf.setDebug(true);
		    conf.setNumWorkers(2);
		    if (args!= null && args.length >= 2){//first argument should be topologyName
		   		topologyName = args[1];
		   		
		   	}
		    
		    System.out.println("Submitting topology by name: "+topologyName);
		    StormTopology topo =builder.createTopology();
		    if (args != null && args.length > 2) {
		      conf.setNumWorkers(3);

		      StormSubmitter.submitTopology(topologyName, conf, builder.createTopology());
		    }

		    else
		    {
		    			    	
		        LocalCluster cluster = new LocalCluster();
		        cluster.submitTopology(topologyName, conf, builder.createTopology());
		       	//Utils.sleep(30000);
		      	//cluster.killTopology(topologyName);
		      	//cluster.shutdown();
		    }
		  }
	 
	 private static TopologyBuilder CreateProtonTopology(String pathToJson, String topologyName) throws MissingInputConfigurationException{
		 TopologyBuilder builder = new TopologyBuilder();
		 String jsonString = createInputContents(pathToJson);
		 JSONObject parsed=null;	
		 try {
			parsed = ((JSONObject) JSON.parse(jsonString));
		} catch (NullPointerException | IOException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 	
	 	JSONObject root = (JSONObject) parsed.get("epn");
	 	JSONArray events = (JSONArray) root.get("events");
	 	System.out.println(events.size());
		JSONArray producers = (JSONArray) root.get("producers");
		BaseRichSpout inputSpout=null;
		BaseRichBolt formatterBolt=null;
	 	for(Object obj : producers){
 			if(!((String)((JSONObject) obj).get("type")).equals("File")) continue;
 			try{
 				JSONArray properties= (JSONArray) ((JSONObject)obj).get("properties");
 				System.out.printf("There are %d properties\n", properties.size());
 				System.out.println(properties);
 				inputSpout = new FileReaderSpout(properties);
 				formatterBolt = new CsvFormatterBolt(properties, events);
 			}catch(NoFilenameParameterException e){
 				continue;
 			}
 			catch(MissingCSVSectionException e){
 				inputSpout=null;
 				continue;
 			}
 		}
	 	if(inputSpout == null || formatterBolt==null) throw new MissingInputConfigurationException();
	 	EepFacade eep=null;
	 	STORMMetadataFacade facade=null;
		try {
			eep = new EepFacade();
			facade = new STORMMetadataFacade(jsonString,eep);
		} catch (ParsingException | EEPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FacadesManager facadesManager = new FacadesManager();
		facadesManager.setEepFacade(eep);
		
		
		
    		
    	TimerServiceFacade timerServiceFacade = new TimerServiceFacade();
        facadesManager.setTimerServiceFacade(timerServiceFacade);
        WorkManagerFacade workManagerFacade = new WorkManagerFacade();
        facadesManager.setWorkManager(workManagerFacade);
		
        
		builder.setSpout(INPUT_NAME, inputSpout);
		builder.setBolt(FORMATTING_BOLT_NAME, formatterBolt).shuffleGrouping(INPUT_NAME);
		builder.setBolt(ROUTING_BOLT_NAME, new RoutingBolt(facadesManager,facade)).
		 
				shuffleGrouping(FORMATTING_BOLT_NAME).
				shuffleGrouping(EPA_MANAGER_BOLT_NAME, STORMMetadataFacade.EVENT_STREAM);
		builder.setBolt(CONTEXT_BOLT_NAME, new ContextBolt(facadesManager,facade)).
			fieldsGrouping(ROUTING_BOLT_NAME, STORMMetadataFacade.EVENT_STREAM,
					new Fields(STORMMetadataFacade.AGENT_NAME_FIELD,STORMMetadataFacade.CONTEXT_NAME_FIELD));
		builder.setBolt(EPA_MANAGER_BOLT_NAME, new EPAManagerBolt(facadesManager,facade)).
			fieldsGrouping(CONTEXT_BOLT_NAME, STORMMetadataFacade.EVENT_STREAM,
					new Fields(STORMMetadataFacade.AGENT_NAME_FIELD,STORMMetadataFacade.CONTEXT_PARTITION_FIELD));
		//TODO create some stuff for flexible output.
		//builder.setBolt(outputBoltName, outputBolt).shuffleGrouping(ProtonTopologyBuilder.ROUTING_BOLT_NAME,STORMMetadataFacade.CONSUMER_EVENTS_STREAM);
		builder.setBolt(OUTPUT_BOLT_NAME, new TestOutputBolt(topologyName)).
			shuffleGrouping(ROUTING_BOLT_NAME,STORMMetadataFacade.CONSUMER_EVENTS_STREAM);
		
		return builder;
	 }


	private static String createInputContents(String inputFileName) {
		String line;
		 StringBuilder sb = new StringBuilder();
	     BufferedReader in = null;	   
	     try
	     {
	    	 in = new BufferedReader(new InputStreamReader(new FileInputStream(inputFileName), "UTF-8"));	    
	    	 while ((line = in.readLine()) != null)
	    	 {
	    		 sb.append(line);
	    	 }

	     }catch(Exception e)
	     {
	    	 e.printStackTrace();
	     }finally
	     {
	    	 try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	     }
	     
	     return sb.toString();
	}
}
