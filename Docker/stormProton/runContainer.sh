#!/bin/bash
CONTAINER_NAME=proton_storm
docker build -t $CONTAINER_NAME .
docker run -t $CONTAINER_NAME &
sleep 60
CONTAINER_ID=$(docker ps -l | tail -n 1 | cut -d " " -f1)
docker kill $CONTAINER_ID
docker cp $CONTAINER_ID:/data/testoutput.txt ./
#$(docker inspect --format {{.State.Pid}} $CONTAINER_NAME) --mount --uts --ipc --net --pid

