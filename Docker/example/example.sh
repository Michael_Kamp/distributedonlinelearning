#!/bin/bash
git clone https://bitbucket.org/sbothe-iais/ferari.git
cd ferari
mvn install -DskipTests=true
cd examples/mobileFraud
cp /data/MultiAntenna.txt /data/geometricmethod.properties /data/topology.properties /data/DistributedCounter.json src/main/resources/
mvn compile assembly:single -DstormScope=provided
bash -c "redis-server &> /dev/null &"
mvn compile
mvn exec:java -Dstorm.topology=eu.ferari.examples.mobileFraud.Main -Dexec.args="proton src/main/resources/DistributedCounter.json topology.properties  geometricmethod.properties" | tee /data/containerOutput.txt
