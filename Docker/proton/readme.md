This is a docker configuration for runing Proton.
It does not include the necessary .war files.
They can be found at https://ok-jira.iais.fraunhofer.de/wiki/pages/viewpage.action?title=Proton+mobile+fraud+demo&spaceKey=FER
Please add them before building the container locally.

```docker build -t proton .```

to run use ```docker run proton```

Opening a bash inside the container:
```docker run -i -t proton bash```

Change the Sending delay parameter of the file Producer in mobileFraud.json to control data emission speed.

Output is either in the docker container in the file "/usr/local/tomcat/webapps/mobileFraudOutput.txt" or accessible via the dashboard at CONTAINER_IP:8080/dashboard/dashboardMain.html
The first is only available if you run bash inside the container and start ```catalina.sh start``` manually.
