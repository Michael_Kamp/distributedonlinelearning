package eu.ferari.learning;

import java.io.Serializable;
/**
 * an interface for a generic data representation that arrives from a stream
 * @author Rania
 *
 */
public interface DataInstance extends Serializable {

}
