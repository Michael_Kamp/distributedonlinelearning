package eu.ferari.learning.model.kernelFunc;

import java.util.Map;

import eu.ferari.DataPair;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.model.tmp.BaseKernelFunction;

/**
 *  Radial basis function kernel
 * @author Rania
 *
 * @param <T>
 */
public class GaussianKernel extends BaseKernelFunction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2416124694461465248L;
	
	private double gaussianWidth = 2 * Math.pow(638, 2); //296 
	
	public GaussianKernel(){
		
	}
	
	public GaussianKernel(double gaussianWidth){
		this.gaussianWidth = gaussianWidth;
	}
	
	@Override
	public double computeValue(DataPair<RealValuedVector, Double> supportVectorVal, double[] instance) {
		return computeGaussian(supportVectorVal, instance);
	}

	public double computeGaussian(DataPair<RealValuedVector, Double> supportVectorVal, double[] instance) {
			RealValuedVector supportVector = supportVectorVal.getData1();
			double val = supportVector.distanceSquare(instance);
			val = Math.exp(-val/gaussianWidth);
			return val;
	}
	
	public void setParameters(Map<String, Double> parametersMap) {
			gaussianWidth = parametersMap.get("gaussianWidth");	
		}
}