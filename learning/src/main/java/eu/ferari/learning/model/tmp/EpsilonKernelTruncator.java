package eu.ferari.learning.model.tmp;

import java.util.Iterator;
import java.util.Map.Entry;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

public class EpsilonKernelTruncator extends KernelTruncator {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8615684954980681657L;
	/**
	 * a threshold used to determine which support vectors should be truncated based on their alpha value
	 */
	private double epsilon = 0.00001;
	private static final int TRUNCATE_ITERATIONS = 100; 
	private int countForTruncate = 0;
	
	public EpsilonKernelTruncator() {
		
	}
	
	public EpsilonKernelTruncator(double epsilon) {
		this.epsilon  = epsilon;
	}

	@Override
	public void truncateModel(IVector<RealValuedVector, Double> supportVectors) {
		++countForTruncate;
		if (countForTruncate > TRUNCATE_ITERATIONS) {
			countForTruncate = 0;
			Iterator<Entry<RealValuedVector, Double>> it = supportVectors
					.iterator();
			while (it.hasNext()) {
				Entry<RealValuedVector, Double> supportVectorVal = it.next();
				double value = supportVectorVal.getValue();
				if (Math.abs(value) < epsilon) {
					it.remove();
				}
			}
		}
	}
}