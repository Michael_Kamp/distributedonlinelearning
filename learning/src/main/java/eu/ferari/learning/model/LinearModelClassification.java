package eu.ferari.learning.model;

import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.base.LinearModel;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import eu.ferari.learning.updateRules.UpdateRuleBase;

/**
 * (Binary) classification linear model
 * @author Rania
 *
 */
public class LinearModelClassification extends LinearModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2939965826419620070L;

	public LinearModelClassification(UpdateRuleBase<Integer, Double> updater,
			LossFunctionType lossType) {
	
		super(updater, lossType);
	}

	@Override
	public double getDoubleValueOfInstance(Object[] instance, int index) {
		return ((Boolean) instance[index] ? 1: 0);
	}
	@Override
	protected void evaluatePrediction(LearningInstance instance) {
		Boolean yHat = instance.getPredictionScore() > 0;
		boolean isCorrect = yHat.equals ((Boolean)(instance.getTarget()));
		instance.setPredCorrect(isCorrect);
		System.out.println("prediction," + yHat + ",trueValue," + instance.getTarget() +",: " + isCorrect);
		//logger.log(Level.INFO, "prediction: " + yHat + ", trueValue: " + instance.getTarget());
		//logger.log(Level.INFO , "prediction," + yHat + ",trueValue," + instance.getTarget()+","+annotation);
		}
	
	
	
	
}


