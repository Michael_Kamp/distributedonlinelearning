package eu.ferari.learning.model.base;



import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.tmp.LossFunctionFactory;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import eu.ferari.learning.updateRules.UpdateRuleBase;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;
/**
 * 
 * @author Rania
 *
 * @param <T>: the target type, the supported types are Double in case of regression or Boolean in case of binary.  
 */
public abstract class LinearModel extends GenericModel <Integer, Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5134134680579614103L;



	/**
	 * 
	 * @param updater: the updater type
	 * @param lossType: the type of the loss function
	 */
	public LinearModel(UpdateRuleBase<Integer, Double> updater, LossFunctionType lossType) {
			super(updater);
	}

	@Override
	public void predictInstance(LearningInstance instance) {
		if(weights == null) {
			weights = new RealValuedVector(instance.getInstance().length+1);
			updater.initWeights(this);
		}
		double prediction = computePredictionScore(instance);
		instance.setPredictionScore(prediction);
		evaluatePrediction(instance);	
	}

	
	@Override
	/**
	 * get the prediction score of the model
	 * @param instance 
	 * @return
	 */
	public double computePredictionScore(LearningInstance instance) {
		double predictionValue = 0; // the perceptron output
		for(int i = 0 ; i < instance.getInstance().length; i++) {
			double x = getDoubleValueOfInstance(instance.getInstance(), i);
			predictionValue+=  x * weights.get(i+1);
		}
		predictionValue+= weights.get(0); // the threshold w0
		return predictionValue;
	}


	public void printWeights() {
		System.out.println(weights.toString());
		
	}
	
	
	public void updateWeight(Integer i, Double updatedWeight) {
		weights.set(i, updatedWeight);
	}
	@Override
	public void setSyncModel(IVector<Integer, Double> modelValues) {
		super.setSyncModel(modelValues);
		System.out.println("printing out the local node weights after sync:");
		printWeights();
	}
	public abstract double getDoubleValueOfInstance(Object[] instance, int index); 
			
}
