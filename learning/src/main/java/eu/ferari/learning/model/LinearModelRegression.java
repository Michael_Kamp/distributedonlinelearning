package eu.ferari.learning.model;

import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.base.LinearModel;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import eu.ferari.learning.updateRules.UpdateRuleBase;

public class LinearModelRegression extends LinearModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1220674657319353578L;
	private double epsilon = 0.01;

	public LinearModelRegression(UpdateRuleBase<Integer, Double> updater,
			LossFunctionType lossType) {
		super(updater, lossType);
		
	}
	
	@Override
	public double getDoubleValueOfInstance(Object[] instance, int index) {

		return (Double)instance[index];	
	}

	protected void evaluatePrediction(LearningInstance instance) {
		double yHat = instance.getPredictionScore();
		double diff = Math.abs((Double) (instance.getTarget()) - (double) yHat);
		boolean isCorrect = diff < epsilon ;
		instance.setPredCorrect(isCorrect);
		System.out.println("prediction," + yHat + ",trueValue," + instance.getTarget() +",: " + isCorrect);
		//logger.log(Level.INFO, "prediction: " + yHat + ", trueValue: " + instance.getTarget());
		//logger.log(Level.INFO , "prediction," + yHat + ",trueValue," + instance.getTarget()+","+annotation);
		}



}
