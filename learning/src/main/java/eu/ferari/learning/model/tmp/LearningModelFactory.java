package eu.ferari.learning.model.tmp;

import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.model.KernelModelClassification;
import eu.ferari.learning.model.KernelModelRegression;
import eu.ferari.learning.model.LinearModelClassification;
import eu.ferari.learning.model.LinearModelRegression;
import eu.ferari.learning.model.base.ILearningModel;
import eu.ferari.learning.model.tmp.LearningParamsMap.LearningParameter;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;
import eu.ferari.learning.updateRules.KernelStochasticGradientDescentClassification;
import eu.ferari.learning.updateRules.KernelStochasticGradientDescentRegression;
import eu.ferari.learning.updateRules.StochasticGradientDescent;
import eu.ferari.learning.updateRules.UpdateRuleBase;

/**
 * Return an instance of the specified model method.
 * @author Rania
 *
 */
public class LearningModelFactory {
	
	public enum ModelType{LinearRegression, LinearClassification,  KernelRegression, KernelClassification}
	
	public static  <Index, Value> ILearningModel <Index, Value> getModel(ModelType modelType, LossFunctionType lossType, LearningParamsMap params){
		switch (modelType) {
		case LinearRegression :
			double learningRate = params.getParamDouble(LearningParameter.LinearLearningRate);
			UpdateRuleBase<Integer, Double> updater = new StochasticGradientDescent(lossType, learningRate);
			ILearningModel <Index, Value> model  = (ILearningModel<Index, Value>) new LinearModelRegression(updater, lossType);
			return model;
		case LinearClassification :
			learningRate = params.getParamDouble(LearningParameter.LinearLearningRate);
			updater = new StochasticGradientDescent(lossType, learningRate);
			model  = (ILearningModel<Index, Value>) new LinearModelClassification(updater, lossType);
			return model;
		case KernelRegression:
			UpdateRuleBase<RealValuedVector, Double> updater1 = new KernelStochasticGradientDescentRegression(lossType, params);
			model = (ILearningModel<Index, Value>) new KernelModelRegression(updater1, params);
			return model;
		case KernelClassification:
			updater1 = new KernelStochasticGradientDescentClassification(lossType, params);
			model = (ILearningModel<Index, Value>) new KernelModelClassification(updater1, params);
			return model;
		default: 
			return null;
		}
	}

	public static  <Index, Value> ILearningModel <Index, Value> getModel(ModelType modelType, LossFunctionType lossType){
		return getModel(modelType, lossType, new LearningParamsMap());
	}
}