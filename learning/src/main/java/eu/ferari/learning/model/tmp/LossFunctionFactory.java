package eu.ferari.learning.model.tmp;

import eu.ferari.core.utils.GMLConfiguration;
import eu.ferari.learning.interfaces.ILossFunction;
import eu.ferari.learning.lossFunctions.EpsilonInsensitiveLoss;
import eu.ferari.learning.lossFunctions.HingeLoss;
import eu.ferari.learning.lossFunctions.SquaredLoss;

/**
 * a factory class that creates an updater type based on the type that the user gives
 * @author Rania
 *
 */
public class LossFunctionFactory {
	
	public enum LossFunctionType {SquareError, HingeLoss, EpsilonInsensitive};
	
	public static ILossFunction getLossFunction(LossFunctionType lossFunctionType){
		GMLConfiguration config = new GMLConfiguration();
		switch (lossFunctionType) {
		case SquareError : 
			return new SquaredLoss(config);
		case HingeLoss:
			return new HingeLoss(config);
		case EpsilonInsensitive:
			return new EpsilonInsensitiveLoss(config);
		default: 
			return null;
		}
	}
	
	
	

}
