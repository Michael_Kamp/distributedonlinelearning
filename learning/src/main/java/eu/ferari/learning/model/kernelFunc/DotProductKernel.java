package eu.ferari.learning.model.kernelFunc;

import eu.ferari.DataPair;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.model.tmp.BaseKernelFunction;

/**
 * A linear (dot product) kernel model
 * @author Rania
 *
 * @param <T>
 */
public class DotProductKernel  extends BaseKernelFunction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2416124694461465248L;

	@Override
	public double computeValue(DataPair<RealValuedVector, Double> supportVectorVal, double[] instance) {
		
		return computeDotProduct(supportVectorVal, instance);
	}

	public double computeDotProduct(DataPair<RealValuedVector, Double> supportVectorVal, double[] instance) {
			RealValuedVector supportVector = supportVectorVal.getData1();
			double dotProd = supportVector.innerProduct(instance);
			return dotProd;
	}
	
		
}