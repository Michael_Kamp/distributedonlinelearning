package eu.ferari.learning.model.tmp;

import java.io.Serializable;
import java.util.Map;

import eu.ferari.DataPair;
import eu.ferari.core.utils.RealValuedVector;

/**
 * 
 * @author Rania
 *
 * @param <T> the target type (classification/regression)
 */
public abstract class BaseKernelFunction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7053290126501809976L;
	/**
	 * compute the prediction value based on the kernel function (for example, dot product or polynomial)
	 * @param weights
	 * @param instance
	 * @return
	 */
	

	public abstract double computeValue(DataPair<RealValuedVector, Double> supportVectorVal, double[] instance);


	
	public void setParameters(Map<String, Double> parametersMap) {
		
	}
}