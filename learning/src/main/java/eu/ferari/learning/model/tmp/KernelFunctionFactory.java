package eu.ferari.learning.model.tmp;

import eu.ferari.learning.model.kernelFunc.DotProductKernel;
import eu.ferari.learning.model.kernelFunc.GaussianKernel;
import eu.ferari.learning.model.kernelFunc.PolynomialKernel;
import eu.ferari.learning.model.tmp.LearningParamsMap.LearningParameter;

public class KernelFunctionFactory {
	
	public enum KernelFunctionType {Linear, Polynomial, Gaussian};
	
	public static BaseKernelFunction getKernelFunction(KernelFunctionType kernelFunction, LearningParamsMap params){
		switch (kernelFunction) {
		case Linear : 
			return new DotProductKernel();
		case Polynomial:
			int polyDegree = params.getParamInt(LearningParameter.PolynomialKernelDegree);
			return new PolynomialKernel(polyDegree);
		case Gaussian:
			return new GaussianKernel();
		default: 
			return null;
		}
	}
	
	
}