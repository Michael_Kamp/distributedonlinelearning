package eu.ferari.learning.model;

import java.lang.reflect.InvocationTargetException;

import eu.ferari.core.utils.GMLConfiguration;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.base.KernelModel;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.updateRules.UpdateRuleBase;

public class KernelModelRegression extends KernelModel<Boolean>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7342291920988990569L;
	private double epsilon = 0.001;
	public KernelModelRegression(GMLConfiguration config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException {
		super(config);
	}
	
	public KernelModelRegression(UpdateRuleBase<RealValuedVector, Double> updater, LearningParamsMap params) {
		super(updater, params);
	}
	
	
	@Override
	protected void evaluatePrediction(LearningInstance instance) {
		double yHat = instance.getPredictionScore();
		double diff = Math.abs((Double) (instance.getTarget()) - (double) yHat);
		boolean isCorrect = diff < epsilon  ;
		instance.setPredCorrect(isCorrect);
		System.out.println("prediction," + yHat + ",trueValue," + instance.getTarget() +",: " + isCorrect);
		
		}
	
	
	protected double[] getDoubleVector(Object[] instance) {
		double[] vector = new double[instance.length];
		
		for (int i = 0; i < instance.length; i++) {
			vector[i] = ((Double) instance[i]);
		}
	return vector;
}

	

}
