package eu.ferari.learning.model;

import java.lang.reflect.InvocationTargetException;

import eu.ferari.core.utils.GMLConfiguration;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.base.KernelModel;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.updateRules.UpdateRuleBase;

public class KernelModelClassification extends KernelModel<Boolean>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7342291920988990569L;

	public KernelModelClassification(GMLConfiguration config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException {
		super(config);
	}
	
	public KernelModelClassification(UpdateRuleBase<RealValuedVector, Double> updater, LearningParamsMap params) {
		super(updater, params);
	}
	
	
	@Override
	protected void evaluatePrediction(LearningInstance instance) {
		Boolean yHat = instance.getPredictionScore() > 0;
		boolean isCorrect = yHat.equals ((Boolean)(instance.getTarget()));
		instance.setPredCorrect(isCorrect);
		System.out.println("prediction," + yHat + ",trueValue," + instance.getTarget() +",: " + isCorrect);
		
		}
	
	@Override
	protected double[] getDoubleVector(Object[] instance) {
		double[] vector = new double[instance.length];
		for (int i = 0; i < instance.length; i++) {
			vector[i] = ((Boolean) instance[i]) ? 1.0 : 0.0;
		}
		return vector;
	}

}
