package eu.ferari.learning;

import java.util.Arrays;

public class LearningInstance implements DataInstance{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 318055455827238839L;
	private Object [] instance;
	private Object target;
	private boolean isPredCorrect;

	

	private double predictionScore;
	private double lossValue;
	
	public LearningInstance(Object [] instance) {
		target = instance[instance.length-1];
		this.instance = Arrays.copyOfRange(instance, 0 , instance.length-1); 
	}
	
	public double getPredictionScore() {
		return predictionScore;
	}
	public void setPredictionScore(double predictionScore) {
		this.predictionScore = predictionScore;
	}
	public double getLossValue() {
		return lossValue;
	}
	public void setLossValue(double lossValue) {
		this.lossValue = lossValue;
	}

	public Object[] getInstance() {
		return instance;
	}
	
	public Object getTarget() {
		return target;
	}
	
	
	public boolean isPredCorrect() {
		return isPredCorrect;
	}

	public void setPredCorrect(boolean isPredCorrect) {
		this.isPredCorrect = isPredCorrect;
	}

}
