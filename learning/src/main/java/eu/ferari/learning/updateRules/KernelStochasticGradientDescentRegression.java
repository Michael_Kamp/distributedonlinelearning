package eu.ferari.learning.updateRules;

import java.lang.reflect.InvocationTargetException;

import eu.ferari.core.utils.GMLConfiguration;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;

public class KernelStochasticGradientDescentRegression extends KernelStochasticGradientDescent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5140824407187604830L;

	KernelStochasticGradientDescentRegression(GMLConfiguration config) throws InstantiationException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException,
			SecurityException, ClassNotFoundException {
		super(config);
	}
	
	public KernelStochasticGradientDescentRegression(LossFunctionType lossType,
			LearningParamsMap params) {
		super(lossType, params);
	}

	@Override
	protected RealValuedVector getNewSV(LearningInstance  dataInst) {
		RealValuedVector newSV = new RealValuedVector((Double[]) dataInst.getInstance());
		return newSV;
		
	}
}
