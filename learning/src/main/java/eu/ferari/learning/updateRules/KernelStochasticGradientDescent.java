package eu.ferari.learning.updateRules;


import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import eu.ferari.core.utils.GMLConfiguration;
import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.model.base.ILearningModel;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.model.tmp.LearningParamsMap.LearningParameter;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;

/**
 * This update rule is based on the paper "Online learning with kernels by Jyrki
 * Kivinien et al.
 * 
 * @author Rania
 *
 */
public abstract class KernelStochasticGradientDescent extends UpdateRuleBase<RealValuedVector, Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3854265680684484718L;
	private double learningRate;
	private double regularizer;
	private double updateFactor;

	KernelStochasticGradientDescent(GMLConfiguration config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException {
		super(config);
		this.regularizer = Double.parseDouble(config.updateRuleParams.get(0).get(String.valueOf(LearningParameter.Regularizer)));//0.00000005; // regularization parameter
		this.learningRate = Double.parseDouble(config.updateRuleParams.get(0).get(String.valueOf(LearningParameter.KernelLearningRate)));//0.00000005; // regularization parameter
		updateFactor = 1 - regularizer * learningRate;
	}

	public KernelStochasticGradientDescent(LossFunctionType lossType, LearningParamsMap params) {
		super(lossType);
		this.regularizer = params.getParamDouble(LearningParameter.Regularizer);//0.00000005; // regularization parameter
		this.learningRate = params.getParamDouble(LearningParameter.KernelLearningRate);//0.32;
		updateFactor = 1 - regularizer * learningRate;
	}

	@Override
	public void updateModel(ILearningModel<RealValuedVector, Double> kernelModel, LearningInstance dataInst) {
		RealValuedVector newSV = getNewSV(dataInst);
		// first update all existing kernels
		kernelModel.getWeights().scalarMultiply(updateFactor);
		// the new kernel to be added
		double kernelWeight = 0;
		if (kernelModel.getWeights().containsIndex(newSV)) {
			kernelWeight = updateFactor * kernelModel.getWeights().get(newSV);
		}
		kernelWeight -= learningRate * lossFunction.evaluateLossDerivative(dataInst, 1);
		if(kernelWeight != 0)
			kernelModel.getWeights().set(newSV, kernelWeight);
		
	}
	
	protected abstract RealValuedVector getNewSV(LearningInstance dataInst);

	/**
	 * we start with an empty set of support vectors
	 */
	@Override
	public void initWeights(ILearningModel<RealValuedVector, Double> model) {

	}
	
	@Override
	public void setParameters(Map<String, Double> parametersMap) {
		learningRate = parametersMap.get("learningRate");
		regularizer = parametersMap.get("regularizer");
	}


}