package eu.ferari.learning.lossFunctions;

import java.util.Map;

import eu.ferari.core.utils.GMLConfiguration;
import eu.ferari.learning.LearningInstance;
import eu.ferari.learning.interfaces.ILossFunction;
/**
 * this epsilon insensitive loss function may only be used in regression. defined as L_eps(y,t) = max {0, |y-t| - epsilon}  
 * @author Rania
 *
 */
public class EpsilonInsensitiveLoss implements ILossFunction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7440693918831979466L;
	private double epsilon = 0.4;
	
	public EpsilonInsensitiveLoss(GMLConfiguration config) {
		if (config.lossFunction != null && !config.lossFunction.isEmpty()) {
			for (Map<String,String> paramMap : config.lossFunctionParams) {
				if (paramMap.containsKey("epsilon")) {
					this.epsilon = (float)Float.parseFloat(paramMap.get("epsilon"));
				}
			}
		}
	}
	
	public EpsilonInsensitiveLoss(double epsilon) {
		this.epsilon = epsilon;
	}

	@Override
	public double evaluateLossDerivative(LearningInstance dataInst, double xValue) {
		double trueValue = (Double)dataInst.getTarget();
		double prediction = (Double)dataInst.getPredictionScore(); 
		if(Math.abs(prediction - trueValue) <= epsilon)
			return 0;
		if(prediction - trueValue < 0)
			return -1;
		return 1;
	}

	@Override
	public double evaluateLossValue(LearningInstance dataInst) {
		return Math.max(0, Math.abs((Double)dataInst.getTarget() - (Double)dataInst.getPredictionScore()) - epsilon);
	}

}
