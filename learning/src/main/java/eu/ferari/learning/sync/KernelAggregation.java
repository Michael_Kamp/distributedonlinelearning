package eu.ferari.learning.sync;

import java.io.Serializable;
import java.util.Collection;

import eu.ferari.core.interfaces.IAggregationMethod;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

public class KernelAggregation implements IAggregationMethod<RealValuedVector, Double>, Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6638509783322996935L;

	@Override
	public IVector<RealValuedVector, Double> syncWeights(
			Collection<IVector<RealValuedVector, Double>> balancingSetWeights) {
		IVector<RealValuedVector, Double>  singleModel = balancingSetWeights.iterator().next();
		IVector<RealValuedVector, Double>  averagedModel = singleModel.average(balancingSetWeights);
		return averagedModel;
	}

}