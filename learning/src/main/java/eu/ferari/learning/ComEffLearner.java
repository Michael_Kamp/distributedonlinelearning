package eu.ferari.learning;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.IComEffState;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.GMLConfiguration;
import eu.ferari.core.utils.JMSLogAppender;
import eu.ferari.learning.model.base.ILearningModel;
import eu.ferari.learning.model.tmp.LearningModelFactory;
import eu.ferari.learning.model.tmp.LearningModelFactory.ModelType;
import eu.ferari.learning.model.tmp.LearningParamsMap;
import eu.ferari.learning.model.tmp.LossFunctionFactory.LossFunctionType;


/**
 * the learner node
 * @author Rania
 *
 */
public class ComEffLearner <Index, Value> implements IComEffState <Index, Value> {

	
	private ILearningModel <Index, Value> model;
	private double accumLossValue = 0;
	private int instancesCount = 0;
	private int correctPredCount = 0;

	private static final long serialVersionUID = 3563792621962843627L;
	
	private static final Logger logger = Logger.getLogger("learner");
	
	static {
		try {
			//PatternLayout patternLayout = new PatternLayout("%d{HH:mm:ss,SSS} %n%n");
			PatternLayout patternLayout = new PatternLayout("%r [%t] - %m%n");
			logger.addAppender(new FileAppender(patternLayout, "output/loss.out"));
			logger.addAppender(new JMSLogAppender());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public ComEffLearner(GMLConfiguration config) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException {
		this.model = (ILearningModel<Index, Value>)(this.getClass().getClassLoader().loadClass(config.modelType).getConstructor().newInstance(config));
	}
	
	public ComEffLearner(LossFunctionType lossType, ModelType modelType) {
		model = LearningModelFactory.<Index, Value>getModel(modelType, lossType);
	}
	/**
	 * 
	 * @param lossType
	 * @param modelType
	 * @param targetType
	 * @param params learning parameters used by the model such as learning rate, regularization parameters, and so on..
	 */
	public ComEffLearner(LossFunctionType lossType, ModelType modelType, LearningParamsMap params) {
		model = LearningModelFactory.<Index, Value>getModel(modelType, lossType, params);
	}
	
	public void setLogAnnotation(String stAnnotation){ //stupid hack to distinguish logs of different topologies
		model.setLogAnnotation(stAnnotation);
	}

	/**
	 * the learner bolt received an input data
	 */
	@Override
	public void processInput(DataTuple data){
		Object [] instance =  (Object[]) data.getValue(0);
		LearningInstance dataObject = new LearningInstance(instance);
		model.predictInstance(dataObject);
		model.updateModel(dataObject);
		instancesCount++;
		accumLossValue+= dataObject.getLossValue();
		//System.out.println("current loss value: " + dataObject.getLossValue());
		//logger.log(Level.INFO, "loss: " + accumLossValue);
		//double avgLossValue = accumLossValue/instancesCount;
		if(dataObject.isPredCorrect()) 
			correctPredCount++;
		//System.out.println("avg loss value so far: " + avgLossValue);
		System.out.println("loss: " + accumLossValue);
		//System.out.println("accuracy so far: " + (float)correctPredCount/instancesCount);
	}


	@Override
	public IVector <Index, Value>  getLocalVariable() {
		return model.getWeights();
	}

	@Override
	public void setLocalVariable(IVector <Index, Value>  v) {
			model.setSyncModel(v);		
	}

	public void setParameters(Map<String, Double> parametersMap) {
		model.setParameters(parametersMap);
		
	}

	public void processInput(Double[] instance) {
		List<Object> objects = new ArrayList<Object>();
		objects.add(instance);
		DataTuple data = new DataTuple(objects);
		processInput(data);
	}

}