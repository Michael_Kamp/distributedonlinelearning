package eu.ferari.learning.interfaces;

import java.io.Serializable;

import eu.ferari.learning.LearningInstance;

public interface ILossFunction extends Serializable{
	
	public double evaluateLossDerivative(LearningInstance dataInst, double xValue);
	
	public double evaluateLossValue(LearningInstance dataInst);

}
