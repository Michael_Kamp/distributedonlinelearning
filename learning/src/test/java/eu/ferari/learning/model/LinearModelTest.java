package eu.ferari.learning.model;

import org.junit.Before;

import eu.ferari.core.utils.RealValuedVector;
import eu.ferari.learning.LearningInstance;

public class LinearModelTest {

	LearningInstance instance1;
	LearningInstance instance2;
	RealValuedVector weights;
	
	@Before public void init(){
		weights = new RealValuedVector(new Double[]{2.0, 1.0, 1.2, 5.34, -0.5});	
		
		Double [] featuresAndTarget  = new Double[]{0.1, 3.4, 1.23, 2.0, 1.2};
		instance1 = new LearningInstance(featuresAndTarget);
		instance2 = new LearningInstance(new Double[]{1.0, 3.0, 5.0, 2.0, 0.5});
    }

	//TODO
	/**
	 * the expected values of the weights are to be changed, I don't remember which learning rate I calculated them
	 * with and need to recalculate.
	 */
	/*
	@Test public void testLinearModelSquareError(){
		StochasticGradientDescent updater = new StochasticGradientDescent( LossFunctionType.SquareError, TargetType.RealValue);
		LinearModel <Double> model = new LinearModel<Double>(updater, LossFunctionType.SquareError, TargetType.RealValue);
		
		model.predictInstance(instance1);
		
		setWeights(weights, model);
		double result = model.computePredictionScore(instance1);
		double expected = 11.7482;
		assertTrue(Math.abs(result - expected) < 0.0001);
		instance1.setPredictionScore(result);
		model.updateModel(instance1);
		
		RealValuedVector expectedWeights = new RealValuedVector(new Double[]{0.94518, 0.894518, -2.386388, 4.0425714, -2.60964});
		model.getWeights();
		
		for(int i = 0 ; i < expectedWeights.getDimension(); i++) {
    		assertTrue(Math.abs(expectedWeights.get(i) - model.getWeights().get(i)) < 0.01);
    	}
		
	   
	    double expectedLoss = 55.6322;
	    
	    assertTrue(Math.abs(instance1.getLossValue() - expectedLoss) < 0.0001);
	    
	    
	}
	
	
	@Test public void testLinearModelEpsilonInsensitive(){
		StochasticGradientDescent updater = new StochasticGradientDescent( LossFunctionType.EpsilonInsensitive, TargetType.RealValue);
		LinearModel <Double> model = new LinearModel<Double>(updater, LossFunctionType.EpsilonInsensitive, TargetType.RealValue);
		model.predictInstance(instance1);
		
		setWeights(weights, model);
		double result = model.computePredictionScore(instance1);
		double expected = 11.7482;
		assertTrue(Math.abs(result - expected) < 0.0001);
		instance1.setPredictionScore(result);
		model.updateModel(instance1);
		
		RealValuedVector expectedWeights = new RealValuedVector(new Double[]{1.9, 0.99, 0.86, 5.217, -0.7});
		model.getWeights();
		for(int i = 0 ; i < expectedWeights.getDimension(); i++) {
    		assertTrue(Math.abs(expectedWeights.get(i) - model.getWeights().get(i)) < 0.01);
    	}
		   
	    double expectedLoss = 10.1482;
	    
	    assertTrue(Math.abs(instance1.getLossValue() - expectedLoss) < 0.0001);
	}
	*/
	
	
	private void setWeights(RealValuedVector weights, LinearModelRegression model) {
		for(int i = 0 ; i < weights.getDimension(); i++) {
			model.updateWeight(i,  weights.get(i));
		}
		
	}
	
}
