import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ICoordinatorState;
import eu.ferari.core.interfaces.ISend;

/**
 * Created by mofu on 27/10/15.
 */
public class CoordinatorCaller implements ISend {
    private ICoordinatorState state;

    public void signal(DataTuple data) {
        state.update(data);
    }

    public int getTaskId() {
        //TODO
        return 0;
    }

    public CoordinatorCaller(ICoordinatorState state) {
        this.state = state;
    }
}
