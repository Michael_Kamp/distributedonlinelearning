import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ILocalState;
import eu.ferari.core.interfaces.ISend;

/**
 * Created by mofu on 27/10/15.
 */
public class SiteCaller implements ISend {
    private ILocalState state;
    public SiteCaller(ILocalState state) {
        this.state = state;
    }

    public int getTaskId() {
        //TODO
        return 0;
    }

    public void signal(DataTuple data) {
        state.handleFromCommunicator(data);
    }
}
