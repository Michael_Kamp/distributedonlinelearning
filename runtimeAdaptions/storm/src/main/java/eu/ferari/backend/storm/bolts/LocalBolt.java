package eu.ferari.backend.storm.bolts;

import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ILocalState;
import eu.ferari.core.utils.Message;

/**
 * Bolt to handle count distinct.
 * <p>
 * Count the distinct occurrences of an artist in the stream of hearings tuples.
 */
public class LocalBolt extends BaseRichBolt {

    private static final long serialVersionUID = 1L;
    private OutputCollector collector;
    private final ILocalState state;

    public LocalBolt(ILocalState state) {
        this.state = state;
    }

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        state.setSender(new StormSendToCoordinator(collector, context.getThisTaskId()));
    }

	@Override
	public void execute(Tuple tuple) {
		if (tuple.getSourceStreamId().equals(
				StormSendToLocal.TO_LOCAL_STREAM_NAME)) {
			state.handleFromCommunicator((DataTuple)tuple.getValue(0));
		} else {
			state.update(new DataTuple(tuple.getValue(1)));
		}
		collector.ack(tuple);
	}

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME, new Fields(
            StormSendToCoordinator.FIELDNAME));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }

}
