Initial Checkout for Development
================================

This article describes how you can import the FERARI project into eclipse, to start development.
If you use iintellij, you can just use the __Checkout from VCS__ option.

Alternatively you can watch a [Video](http://sbothe-iais.bitbucket.org/ferari/projectImport.mp4)

Prerequisites
--------------
1. Eclipe (Luna or later)
2. Eclipse git plugin
3. Eclipse maven plugin

Number 2 and 3 are already included in the EE verion of Eclipse.
We will show how to install 4 during this tutorial.

Importing the Project
--------------------
Right click in your __Project Explorer__ select __Import -> Import__.

![Import Screenshot](import.png "Import Screenshot")

Browse for the __Maven__ category and select the __Checkout Maven Projects from SCM__ option.

![mvn import from SCM](mvn_import.png "Import Maven Projects from SCM")

Click on __Next__.

If you do not have the git connector for the Maven plugin installed, i.e. you can not select __git__ in the drop down menu next to __SCM URL__,
click on __m2e Marketplace__.

![Install m2e connector](m2e_selection.png "Select m2e Marketplace")

After all entries have loaded, scroll to the bottom and select __m2e-egit__ and click on __finish__.

![Select m2e-egit](m2e_selection_2.png "Select m2e-egit")

Folow the installation process and restart eclipse.

Now you should be able to select __git__ from the drop down menu next to __SCM URL__. Do so.
Enter the URL of the repository __https://bitbucket.org/sbothe-iais/ferari__ in the text field next to it.

![enter the Repository details](mvn_import2.png "Enter the Repository Detals")

Click on __Finish__.

After the import process has finished right lick on the __ferari__ project select __Run As -> Maven install__.

![Run mvn install](mvn_install.png "Run mvn install")


Now everything is set up and you can start working.

### Happy Coding



F.A.Q.
===========================
* This does not work.
    A: Try deleting your local Maven repository __~/.m2/repository__ and start over.

* This still does not work.
    A: Get in touch.
