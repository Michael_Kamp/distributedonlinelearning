package eu.ferari.core.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.gson.JsonObject;
import com.ibm.ferari.client.FerariClientBuilder;
import com.ibm.ferari.client.WSForwarder;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

/**
 * Redis sender implementation for logs
 * @author Rania
 *
 */
public class JMSSender implements ISend {
    
	private final static String infoChannel = "InfoChannel";
	private final static String traceChannel = "TraceChannel";
	private final static String debugChannel = "DebugChannel";
	
	private static final Map<String, String> logLvlTranslation = new HashMap<String, String>();
	
	static {
		logLvlTranslation.put("info", infoChannel);
		logLvlTranslation.put("trace", traceChannel);
		logLvlTranslation.put("debug", debugChannel);
	}
	
	private WSForwarder wsForwarder = FerariClientBuilder.getInstance().createWSForwarder("http://localhost:3001", "learnerEvents");
	private int count = 0;

    public JMSSender() {
    	
    }

	@Override
	public void signal(DataTuple data) {
		try {
			++count;
			LogMessage message = new LogMessage(data);
			String json = message.toJSON();
			System.err.println(json);
			String sender = message.getSource();
			
			String lvl = sender  + logLvlTranslation.get(message.getMessageType());
			JsonObject j = new JsonObject();
			j.addProperty("level", lvl);
			j.addProperty("event", json);
			String event = j.toString();
			wsForwarder.forwardSync(event);
			System.err.println(event);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
/*		//String key = "";
		try {
			publisherJedis = jedisPool.getResource();
			switch (message.getMessageType()) {
				case "info":
					publisherJedis.publish(sender + infoChannel, json);
					System.out.println(sender + infoChannel + " *** " + json.toString());
					break;
				case "trace":
					publisherJedis.publish(sender + traceChannel, json);
					break;
				case "debug":
					publisherJedis.publish(sender + debugChannel, json);
					break;
				default:
					break;
				}
				
				if (count > 1000) {
					count = 0;
					Set<String> keys = publisherJedis.keys("*");
					for (String key : keys) {
						publisherJedis.del(key);
					}
				}

				jedisPool.returnResource(publisherJedis);
			} catch (Exception e) {
				System.err.println("Redis Logging Exception " + e.getMessage());
			}*/
	}

	@Override
	public int getTaskId() {
		return 0;
	}

	
}
