package eu.ferari.core.utils;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;

import eu.ferari.core.interfaces.IVector;

public class RealValuedVector implements IVector<Integer, Double>, Comparable<RealValuedVector> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1674807216027066652L;
	private double[] vector;
	public static double epsilon = 0.000001;
	
	public RealValuedVector() {
	}
	
	public RealValuedVector(int dim) {
		this.vector = new double[dim];
	}

	
	public RealValuedVector(Double[] initVec) {
		this.vector = new double[initVec.length];
		for (int i = 0; i < initVec.length; i++) {
			this.vector[i] = initVec[i];
		}
	}
	
	public RealValuedVector(Boolean[] initVec) {
		this.vector = new double[initVec.length];
		for (int i = 0; i < initVec.length; i++) {
			this.vector[i] = initVec[i] ? 1 : 0;
		}
	}


	@Override
	public void add(IVector<Integer, Double> other) {
		for (int i = 0; i < this.getDimension(); i++) {
			this.vector[i] += other.get(i);
		}
	}

	@Override
	public void subtract(IVector<Integer, Double> other) {
		for (int i = 0; i < this.getDimension(); i++) {
			this.vector[i] -= other.get(i);
		}
	}

	
	@Override
	public void scalarMultiply(double d) {
		for (int i = 0; i < this.getDimension(); i++) {
			this.vector[i] *= d;
		}
	}

	@Override
	/**
	 * calculates the Euclidean distance.
	 */
	public double distance(IVector<Integer, Double> other) {
		double dist = 0.0;
		for (int i = 0; i < this.getDimension(); i++) {
			double diff = this.vector[i] - other.get(i);
			dist += diff*diff;
		}
		return Math.sqrt(dist);
	}
	
	public double distance(double[] other) {
		return Math.sqrt(distanceSquare(other));
	}
	
	public double distanceSquare(double[] other) {
		double distSquare = 0.0;
		for (int i = 0; i < this.getDimension(); i++) {
			double diff = this.vector[i] - other[i];
			distSquare += diff*diff;
		}
		return distSquare;
	}
	
	/**
	 * calculates the dot product
	 */
	public double innerProduct(IVector<Integer, Double> other) {
		double value = 0.0;
		for (int i = 0; i < this.getDimension(); i++) {
			value+= this.vector[i] * other.get(i);
		}
		return value;
	}
	
	public double innerProduct(double[] other) {
		double value = 0.0;
		for (int i = 0; i < this.getDimension(); i++) {
			value+= this.vector[i] * other[i];
		}
		return value;
	}
	
	

	@Override
	public IVector<Integer, Double> copy() {
		RealValuedVector v = new RealValuedVector(this.getDimension());
		for (int i = 0; i < this.getDimension(); i++) {
			v.set(i, this.vector[i]);
		}
		return v;
	}

	@Override
	public Double get(Integer idx) {
		return this.vector[idx];
	}

	@Override
	public void set(Integer idx, Double val) {		
		this.vector[idx] = val;
	}


	@Override
	public void initializeToZeroVector(int dim) {
		vector = new double[dim];
		for (int i = 0; i < dim; i++) {
			this.vector[i] = 0;
		}
	}
	
	@Override
	public void initializeToZeroVector() {
		for (int i = 0; i < this.getDimension(); i++) {
			this.vector[i] = 0;
		}
	}

	@Override
	public void initialise(IVector<Integer, Double> v) {
		vector = new double[v.getDimension()];
		for (int i = 0; i < this.getDimension(); i++) {
			this.vector[i] = v.get(i);
		}
	}

	@Override
	public int getDimension() {
		if(vector == null)
			return -1;
		return vector.length;
	}
	@Override
	public String toString() {
		return Arrays.toString(vector);
	}

	@Override
	public Iterable<Integer> getKeys() {
		return new AbstractList<Integer>(){
			@Override
			public Integer get(int i) {
				return i;
			}

			@Override
			public int size() {
				return vector.length;
			}
		};
	}

	@Override
	public boolean containsIndex(Integer index) {
		if(index < vector.length)
			return true;
		return false;
	}
	
	// the truncation operator will take care of unifying equal vectors or vectors that are not very far off.
	@Override
	public boolean equals(Object other) {
		if(other instanceof RealValuedVector) {
			RealValuedVector otherRvv = (RealValuedVector) other;
			return otherRvv.equals(vector);
		}
		return false;
	}

	public boolean equals(double[] otherVector) {
		return Arrays.equals(vector, otherVector); 
	}
	
		/*@Override really problematic, does not compute consistent values, at least not to doubles that we humans
		 // consider equal, even for simple double array such as 0.0 and 1.0..
		  * the main problem was with retrieving a value from the map by its key value
		  */
	/*public int hashCode() {
			int result=getDimension();
	        long f;
	        for(int j =0; j<getDimension();++j){
	            f= Double.doubleToLongBits(vector[j]);
	            result *=37;
	            result += (int)(f ^ (f >>> 32));
	        }
	        return result;
		}*/
	
	public int hashCode() {
		return Arrays.hashCode(vector);
	}



	@Override
	public int compareTo(RealValuedVector o) {
		return o.equals(this) ? 0 : 1;
	}


	@Override
	public Iterator<Entry<Integer, Double>> iterator() {
	
		return null;
	}


	@Override
	public IVector<Integer, Double> average(Collection<IVector<Integer, Double>> balancingSetWeights) {
		IVector<Integer, Double> averagedModel = new RealValuedVector(vector.length);
		averagedModel.initializeToZeroVector();
		averagedModel.add(balancingSetWeights);
		averagedModel.scalarMultiply(1.0/balancingSetWeights.size());
		return averagedModel;
	}

	public double[] getValue() {
		return vector;
	}

	

}
