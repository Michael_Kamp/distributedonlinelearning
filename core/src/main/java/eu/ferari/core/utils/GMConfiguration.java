package eu.ferari.core.utils;

public class GMConfiguration extends GMConfigBase{
	private static final long serialVersionUID = 2709220053378034723L;
	
	public double defLSVValue;
	public String thresholdType;
	public double threshold;
	public boolean equalityInAboveThresholdRegion;
}
