package eu.ferari.core.utils;


import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;

import eu.ferari.core.DataTuple;
import eu.ferari.core.utils.JMSSender;




public class JMSLogAppender extends AppenderSkeleton {
	private transient JMSSender sender = new JMSSender();
	
	@Override
	protected void append(LoggingEvent event) {
		DataTuple message = new DataTuple(event.getMessage(), System.currentTimeMillis(), "info", event.getLoggerName());
		sender.signal(message);
	}
	
	public void close() {
		
		
	}

	public boolean requiresLayout() {
		return false;
	}

}
