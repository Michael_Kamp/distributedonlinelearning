package eu.ferari.core.utils;

import java.util.List;
import java.util.Map;

public class GMLConfiguration extends GMConfigBase{
	private static final long serialVersionUID = 1L;

	public String initialModel;
	public String synchProtocol;
	public List<Map<String, String>> synchProtocolParams;
	public String resolutionProtocol;
	public List<Map<String, String>> resolutionProtocolParams;
	public String serviceType;
	public String updateRule;
	public List<Map<String, String>> updateRuleParams;
	public String modelType;
	public List<Map<String, String>> modelParams;
	public String lossFunction;
	public List<Map<String, String>> lossFunctionParams;
	public String aggregationMethod;
}
