package eu.ferari.core.utils;

import com.sun.org.apache.xerces.internal.utils.ConfigurationError;

import eu.ferari.core.interfaces.IVector;

/**
 * Created by mofu on 29/08/16.
 */
public enum MessageType {
	Violation ,
	UpdateReferencePoint,
	UpdateLSV,
	RequestLSV,
	SendLSV,
	Threshold,
	RegisterNode,
	Play,
	Pause,
	PauseConfirmation,
	Event,
	Aggregate,
	Configuration
}
