package eu.ferari.core.utils;

import java.io.Serializable;
import java.util.Collection;

import com.google.gson.Gson;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.IVector;

public class Message implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -535015077104105618L;

	private MessageType type;

	private int nodeId;

	private DataTuple payload;

	private String segmentationInfo;
	
	public Message() {
		
	}
	/**
	 * Constructor for generic message without timestamp
	 * @param type
	 * @param nodeId
	 * @param payload
	 */
	public Message(MessageType type, int nodeId, DataTuple payload){
		this(type, payload);
	}
	
	public Message(MessageType type, int nodeId, IVector<?, ?> lsv){
		this(type, lsv);
		this.nodeId = nodeId;
	}
	
	public Message(MessageType type, DataTuple payload){
		this.type = type;
		this.payload = new DataTuple(payload);
	}
	
	public Message(MessageType type, IVector<?, ?> lsv){
		this.type = type;
		this.payload = new DataTuple(lsv);
	}
	
	public Message(MessageType type, int nodeId){
		this.type = type;
		this.nodeId = nodeId;
	}
	
	public Message(DataTuple data){
		this.type = MessageType.valueOf(data.getString(0));
		this.nodeId = data.getInteger(1);
		if(data.getDataLength() > 2)
			this.payload = (DataTuple) data.getValue(2);//new DataTuple(data.asList().subList(2, data.asList().size()));
	}
	
	public Message(MessageType type, Collection<Integer> addressedNodes) {
		this.type = type;
		this.payload = new DataTuple(addressedNodes);
	}
	public Message(MessageType type, IVector<?, ?> averagedModel,
			Collection<Integer> addressedNodes) {
		this.type = type;
		this.payload = new DataTuple(addressedNodes);
		payload.add(averagedModel);
	}
	public DataTuple toDataTuple(){
		DataTuple message = new DataTuple(String.valueOf(type));
		message.add(nodeId);
		if(payload != null)
			message.add(payload);
		return message;
	}
	
	public MessageType getType() {
		return type;
	}

	public int getNodeId() {
		return nodeId;
	}

	public String getPayload() {
		return payload.toString();
	}
	
	public DataTuple getData() {
		return payload;
	}
	
	public String toJSON(){
		Gson gson = new Gson();		
		return gson.toJson(this);
	}
	
	public void setType(MessageType type) {
		this.type = type;
	}
	
	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Type: " + type);
		sb.append(", NodeId: " + nodeId);
		sb.append(", Payload: " + payload.toString());						
		return sb.toString();
	}

	public void addField(Object... fields) {
		for (Object object : fields) {
			payload.add(object);
		}
	}
	
	public IVector<?, ?> getLSV() {
		return (payload.getValue(0) instanceof IVector<?, ?>) ? (IVector<?, ?>)payload.getValue(0) : (IVector<?, ?>)payload.getValue(1);
		
	}


}
