package eu.ferari.core.interfaces;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map.Entry;

public interface IVector <Index, Value> extends Serializable, Iterable<Entry<Index, Value>>{
	
	/**
	 * initialise this vector with another vector's value
	 * @param v
	 */
	public void initialise(IVector <Index, Value> v);
	

	public default void add(IVector<Index, Value> other, double scaleFactor) {
		add(other);
	}
	
	public void add(IVector<Index, Value> other);
	
	
	public void subtract(IVector<Index, Value> other);
	
	public void scalarMultiply(double d);
	
	public double distance(IVector<Index, Value> other);
	
	
	public IVector<Index, Value> copy();
	
	public Value get(Index idx);
	
	public void set(Index idx, Value val);
	
	public void initializeToZeroVector();

	public int getDimension();
	
	public default void add(Collection<IVector<Index, Value>> vlist) {
		for (IVector<Index, Value> currentVec : vlist) {
			this.add(currentVec, 1);
		}
	}
	
	/**
	 * a method that averages the models of the balancing set
	 * @param vlist
	 */
	public IVector<Index, Value> average(Collection<IVector<Index, Value>> vlist);
	
	public Iterable<Index> getKeys();
	

	public boolean containsIndex(Index index);


	void initializeToZeroVector(int dim);
}
