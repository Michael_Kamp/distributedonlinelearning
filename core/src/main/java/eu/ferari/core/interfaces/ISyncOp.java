package eu.ferari.core.interfaces;

import java.io.Serializable;

/**
 * @author Rania
 *
 */
public class ISyncOp <Index, Value> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7000625427158785506L;
	private ILocalSyncOp <Index, Value> localSyncOperator;
	private ICoordTaskOperator <Index, Value> coordSyncOperator;
	
	public ISyncOp(ILocalSyncOp <Index, Value> localSyncOperator, ICoordTaskOperator <Index, Value> coordSyncOperator) {
		this.localSyncOperator = localSyncOperator;
		this.coordSyncOperator = coordSyncOperator;
	}
	
	public ILocalSyncOp<Index, Value> getLocalSyncOp() {
		return localSyncOperator;
	}
	
	public ICoordTaskOperator <Index, Value> getCoordTaskOp(){
		return coordSyncOperator;
	}
	
}
