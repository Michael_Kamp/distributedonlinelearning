package eu.ferari.core.interfaces;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import eu.ferari.core.utils.Message;
import eu.ferari.core.utils.MessageType;

/**
 * An interface that should be implemented by the class that implements related synchronisation/monitoring
 * logic
 * @author Rania
 *
 * @param <Value>
 * @param <Index>
 */
public interface ICoordTaskOperator <Index, Value>extends Serializable {
	
	/**
	 * decide the action that the coordinator should execute
	 * @param allNodes
	 * @param collection
	 * @param msgType
	 * @param senderId
	 * @return
	 */
	public Message decideAction(Set<Integer> allNodes, Collection<IVector<Index, Value>> balancingSet,
			MessageType msgType, int senderId);
}
