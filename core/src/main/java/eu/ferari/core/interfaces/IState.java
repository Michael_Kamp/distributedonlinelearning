package eu.ferari.core.interfaces;

import eu.ferari.core.DataTuple;


public interface IState {
    public void update(DataTuple data);
}
