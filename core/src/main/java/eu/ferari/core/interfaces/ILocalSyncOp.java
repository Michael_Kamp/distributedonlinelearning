package eu.ferari.core.interfaces;

import java.io.Serializable;

public interface ILocalSyncOp <Index, Value>  extends Serializable {
	
	public boolean checkLocalCondition(IVector<Index, Value> lsv);
	public void syncOperator(IVector<Index, Value> var);
	public void setThreshold(double newThreshold);
		
}
