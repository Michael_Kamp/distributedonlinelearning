package eu.ferari.core.interfaces;

import java.io.Serializable;

import org.apache.log4j.Logger;

import eu.ferari.core.DataTuple;

public interface IComEffState <Index, Value> extends Serializable {
	public void processInput(DataTuple data);
	public IVector <Index, Value>  getLocalVariable();
	public void setLocalVariable(IVector<Index, Value>  iVector);
	public static final Logger logger = Logger.getLogger("Local");
}
