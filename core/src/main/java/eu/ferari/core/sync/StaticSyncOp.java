package eu.ferari.core.sync;
import eu.ferari.core.interfaces.IAggregationMethod;
import eu.ferari.core.interfaces.ISyncOp;
import eu.ferari.core.sync.coord.StaticCoordSyncOp;
import eu.ferari.core.sync.local.StaticLocalSyncOp;

public class StaticSyncOp<Index, Value> extends ISyncOp<Index, Value>{

	private static final long serialVersionUID = -4710409931662781279L;

	public StaticSyncOp(int miniBatchSize, IAggregationMethod<Index, Value> aggregationMethod) {
		super(new StaticLocalSyncOp<Index, Value>(miniBatchSize), new StaticCoordSyncOp<Index, Value>(aggregationMethod));
	}

}
