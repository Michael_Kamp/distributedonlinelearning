package eu.ferari.core.sync.coord;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import eu.ferari.core.interfaces.IAggregationMethod;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.IResolutionProtocol;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.JMSLogAppender;
import eu.ferari.core.utils.Message;
import eu.ferari.core.utils.MessageType;

public class DynamicCoordSyncOp <Index, Value> implements ICoordTaskOperator<Index, Value>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1880441040971110436L;
	
	private static final Logger logger = Logger.getLogger("Coord");
	
	static {
		logger.addAppender(new JMSLogAppender());
	}
	
	private IVector<Index, Value> referenceVector; 
	
	/**
	 * a positive value that represents the variance threshold, a shared value among all nodes and the coordinator
	 */
	private double delta;
	
	private Collection <Integer> balancingSet = new HashSet <Integer>();
	
	private IResolutionProtocol resolution;
	
	private boolean alreadyInResolution = false;
	
	private int queryRespondersCount = 0;

	protected IAggregationMethod<Index, Value> sync;
	
	/**
	 * list of queried nodes in the current resolution round
	 */
	private Collection<Integer> queriedNodes = new ArrayList <Integer>(); 
	

	public DynamicCoordSyncOp(double varianceThreshold, IAggregationMethod<Index, Value> sync, IResolutionProtocol resolutionProtocol) {
		this.delta = varianceThreshold;
		this.sync = sync;
		resolution = resolutionProtocol;
	}
	@Override
	public Message decideAction(Set<Integer> allNodes, Collection<IVector<Index, Value>> balancingSetWeights,
			MessageType msgType, int senderId){
		
		boolean isViolation = isVarianceViolated(balancingSetWeights);
		if(msgType == MessageType.SendLSV || queriedNodes.contains(senderId)) {
			++queryRespondersCount;
		}
		if(msgType == MessageType.Violation) {
			balancingSet.add(senderId);
		}
		if (isViolation) {
			if (queryRespondersCount == queriedNodes.size()  || (!alreadyInResolution && msgType == MessageType.Violation)) {
				if(balancingSet.size() != allNodes.size())
				return runResolution(allNodes);
			}
		}
		if(!isViolation || balancingSet.size() == allNodes.size()) {
			IVector<Index, Value> averagedModel = (IVector<Index, Value>) calcGSV(balancingSetWeights);
			//allWeights.clear();
			//CoordLogger.logEvent("printing out the synchronized coordinator weights in the current round: \n" + averagedModel.toString());
			
			Message syncMessage = new Message(MessageType.UpdateLSV, averagedModel, new ArrayList<Integer>(balancingSet));
			if(balancingSet.size() == allNodes.size()) {
				if(referenceVector == null) 
					referenceVector = averagedModel.copy();
				else
					((IVector<Index, Value>)referenceVector).initialise(averagedModel);
				syncMessage.setType(MessageType.UpdateReferencePoint);
				//CoordLogger.logEvent("reference vector in coord sync operator updated");
			}
			resetSync();
			balancingSetWeights.clear();
			return syncMessage; 
		}
		return null; // we're already in resolution and the variance is violated, don't do anything, just wait to receive more replies

	}
	
	
	/**
	 * start the resolution protocol
	 * @param allNodes 
	 * @param balancingSet 
	 * @param sendToLearner
	 */
	private Message runResolution(Collection<Integer> allNodes ) {
		//CoordLogger.logTrace("starting resolution");
		Collection<Integer> currentQueriedNodes = resolution.getNodesForLSVRequest(balancingSet, queriedNodes, allNodes);
		//logger.log(Level.INFO, "current queried nodes " + Arrays.toString(currentQueriedNodes.toArray()));
		balancingSet.addAll(currentQueriedNodes);
		queriedNodes.addAll(currentQueriedNodes);
		//System.out.println("balancing set size is " + balancingSet.size() + ": " + Arrays.toString(balancingSet.toArray()));
		Message resolutionMessage = new Message(MessageType.RequestLSV, new ArrayList<Integer>(currentQueriedNodes));
		alreadyInResolution = true;
		return resolutionMessage;
	}
	

	

	/**
	 * calculate the global averaged model of the given local models 
	 * @param balancingSetWeights
	 * @return
	 */
	public IVector <Index, Value> calcGSV(Collection<IVector<Index, Value>> balancingSetWeights) {
		return sync.syncWeights(balancingSetWeights);
	}
	

	/**
	 * check if the variance threshold delta is exceeded
	 * @return
	 */
	private boolean isVarianceViolated(Collection<IVector<Index, Value>> balancingSetWeights) {
		if(referenceVector == null)
			return true;
		/*
		if(referenceVector.getDimension() == -1) {
			int dim = balancingSetWeights.iterator().next().getDimension();
			referenceVector.initializeToZeroVector(dim);
		}*/
		IVector <Index, Value> averagedWeight = calcGSV(balancingSetWeights);
		double dev = averagedWeight.distance(referenceVector);
		if (dev > delta)
			return true;
		return false;
	}
	
	
	private void resetSync() {
		alreadyInResolution = false;
		resolution.resetResolution();
		queryRespondersCount = 0;	
		balancingSet.clear();
		queriedNodes.clear();
	}
	
}
