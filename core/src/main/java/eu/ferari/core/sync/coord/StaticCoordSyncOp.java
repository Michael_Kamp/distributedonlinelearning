package eu.ferari.core.sync.coord;

import java.util.Collection;
import java.util.Set;

import eu.ferari.core.interfaces.IAggregationMethod;
import eu.ferari.core.interfaces.ICoordTaskOperator;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.Message;
import eu.ferari.core.utils.MessageType;

/**
 * implements a static global synchronisation protocol
 * 
 * @author Rania
 *
 */
public class StaticCoordSyncOp<Index, Value> implements ICoordTaskOperator<Index, Value> {

	protected IAggregationMethod<Index, Value> aggregationMethod;
	private int currentNumNodes;

	/**
	 * 
	 */
	private static final long serialVersionUID = 7299172759293500547L;

	public StaticCoordSyncOp(IAggregationMethod<Index, Value> aggregationMethod) {
		this.aggregationMethod = aggregationMethod;
	}


	@Override
	public Message decideAction(Set<Integer> allNodes, Collection<IVector<Index, Value>> balancingSetWeights,
			MessageType msgType, int senderId) {
		currentNumNodes = allNodes.size();
		if (!checkGlobalConditions(balancingSetWeights)) {
			return null;
		}
		else {
			IVector<Index, Value> averagedModel =  calcGSV(balancingSetWeights);
			Message syncMessage = new Message(MessageType.UpdateLSV, averagedModel);
			return syncMessage;
		}
	}


	/**
	 * calculate the global averaged model of the given local models
	 * @param balancingSetWeights
	 * @return
	 */
	public IVector <Index, Value> calcGSV(Collection<IVector<Index, Value>> balancingSetWeights) {
		return aggregationMethod.syncWeights(balancingSetWeights);
	}
	/**
	 * start the synchronisation only after all the learner local weights have
	 * been received
	 */
	private boolean checkGlobalConditions(Collection<IVector<Index, Value>> balancingSetWeights) {
		if (balancingSetWeights.size() < currentNumNodes) {
			return false;
		} else {
			return true;
		}
	}


}
