package eu.ferari.core.sync;
import eu.ferari.core.interfaces.IAggregationMethod;
import eu.ferari.core.interfaces.IResolutionProtocol;
import eu.ferari.core.interfaces.ISyncOp;
import eu.ferari.core.sync.coord.DynamicCoordSyncOp;
import eu.ferari.core.sync.local.DynamicLocalSyncOp;

public class DynamicSyncOp<Index, Value> extends ISyncOp<Index, Value>{
	private static final long serialVersionUID = -3762083303228726794L;

	public DynamicSyncOp(int numSteps, double varianceThreshold, IAggregationMethod<Index, Value> aggregationMethod, IResolutionProtocol resolutionProtocol) {
		super(new DynamicLocalSyncOp<Index, Value>(numSteps, varianceThreshold), new DynamicCoordSyncOp<Index, Value>(varianceThreshold, aggregationMethod, resolutionProtocol));
	}
}
