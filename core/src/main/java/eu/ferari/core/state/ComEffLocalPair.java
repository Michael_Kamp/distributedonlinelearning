package eu.ferari.core.state;

import java.io.Serializable;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.IComEffState;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.IVector;

/**
 * Created by mofu on 07/10/15.
 */
public class ComEffLocalPair<Index, Value> implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = -7323947328871241034L;
	private Boolean isInSyncAlready =false;
    private ILocalSyncOp<Index,Value> localSyncOp;
    private IComEffState<Index, Value> state;

    public ComEffLocalPair(IComEffState<Index,Value> state, ILocalSyncOp<Index,Value> syncOp){
        this.localSyncOp = syncOp;
        this.state = state;
    }
    public Boolean isInSyncAlready() {
        return isInSyncAlready;
    }

    public  IVector<Index,Value> getRepresentation(){
			return state.getLocalVariable();
    }


    void processInput(DataTuple data){
        state.processInput(data);
    }

    boolean hasViolation(){
        return localSyncOp.checkLocalCondition(state.getLocalVariable());
    }

    void updateReferencePoint(IVector<Index,Value> vector, boolean updateReference){
        state.setLocalVariable(vector);
     	if(updateReference) {
     		localSyncOp.syncOperator(vector);
     	}
    }
    /**
     * signals to the learner that the synchronisation cycle has ended
     */
    public void setFinishedSync(){
        isInSyncAlready=false;
    }
    public void startSync(){
        isInSyncAlready = true;
    }
   
}
