package eu.ferari.core.state;

import eu.ferari.core.ConfigFactory.LocalSyncOpFactory;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ILocalState;
import eu.ferari.core.interfaces.ILocalSyncOp;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.Message;
import eu.ferari.core.utils.MessageType;

import static eu.ferari.core.utils.MessageType.*;

import java.util.HashMap;


/**
 * Created by mofu on 29/08/16.
 */
public class GatekeeperState implements ILocalState {
	private HashMap<String, HashMap<String,ILocalSyncOp>> functions =new HashMap<>();
	private HashMap<String, String> configurations;
	private HashMap<String,ILocalSyncOp> currentFunction;
	private ILocalSyncOp currentSyncOp;
	private Message currentMessage;
	private ISend sendToCommunicator;

	@Override
	public void update(DataTuple data) {
		String[] splitted=data.getString(1).split(";");
		currentSyncOp=getSyncOp(splitted[0],splitted[1]);
		switch (((MessageType) data.getValue(0))){
			case Aggregate:
				if(currentSyncOp.checkLocalCondition((IVector)data.getValue(2))){
					sendToCommunicator.signal(new DataTuple(Violation,data.getString(1),data.getValue(2)));
				}
				break;
		}
	}

	ILocalSyncOp initFunction(String config){
		return LocalSyncOpFactory.createLocalSyncOp(config);
	}

	ILocalSyncOp getSyncOp(String functionName, String segmentationInfo){
		currentFunction=functions.get(functionName);
		if(currentFunction==null) {
			String config = configurations.get(functionName);
			if(config==null)
				return null; //no config available
			currentFunction=new HashMap<>();
			functions.put(functionName, currentFunction);
		}
		currentSyncOp= currentFunction.get(segmentationInfo);
		if(currentSyncOp==null){
			currentSyncOp=initFunction((functionName));
			currentFunction.put(segmentationInfo,currentSyncOp);
		}
		return currentSyncOp;
	}

	@Override
	public void setSender(ISend sender) {
		sendToCommunicator = sender;
	}

	@Override
	public void handleFromCommunicator(DataTuple data) {
		String[] splitted=data.getString(1).split(";");
		switch((MessageType)data.getValue(0)) {
			case Configuration:
				configurations.put(splitted[0], data.getString(2));
				break;
			default:
				currentSyncOp = getSyncOp(splitted[0], splitted[1]);
				if (currentSyncOp == null) return;
				switch ((MessageType) data.getValue(0)) {
					case Threshold:
						currentSyncOp.setThreshold((Double) data.getValue(2));
						break;
					case UpdateReferencePoint:
						currentSyncOp.syncOperator((IVector) data.getValue(2));
						break;
				}
		}
	}
}
