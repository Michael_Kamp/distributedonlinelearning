package eu.ferari.core.monitoring.syncOps;

import java.util.Random;

import eu.ferari.core.interfaces.IVector;

/**
 * Created by mofu on 30/08/16.
 */
	public class RandomCheck extends ConditionCheck {
    Random rand = new Random();

    public RandomCheck(int estimateLength,float threshold, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold){
        super(threshold, estimateLength,equalityInAboveThresholdRegion,defLSVValue,dynamicThreshold);
    }
    @Override
    double F(IVector<Integer, Double> lsv) {
        return 0;
    }

    @Override
    public boolean checkLocalCondition(IVector<Integer, Double> lsv) {
        return rand.nextDouble() < threshold;
    }

}
