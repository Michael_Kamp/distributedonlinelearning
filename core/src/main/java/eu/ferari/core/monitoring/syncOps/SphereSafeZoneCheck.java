package eu.ferari.core.monitoring.syncOps;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * Created by mofu on 30/08/16.
 */
public class SphereSafeZoneCheck extends SafeZoneCheck {

    public SphereSafeZoneCheck( float threshold, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold) {
        super(threshold, 2, equalityInAboveThresholdRegion, defLSVValue, dynamicThreshold);
    }

    @Override
    protected void computeSafeZones() {
        RealValuedVector factors = new RealValuedVector(refrencePoint.getDimension()+ 1);
        RealValuedVector powers = new RealValuedVector(refrencePoint.getDimension() + 1);
        if (F(refrencePoint) < threshold) {
//            StormOutputWriter.Instance().print("Estimate is inside function area");

            safeZones = new SafeZone[1];
            for (int i = 0; i < refrencePoint.getDimension(); i++) {
                factors.set(i,1.0);
                powers.set(i,2.0);
            }
            factors.set( refrencePoint.getDimension(),0.0-threshold);
            powers.set(refrencePoint.getDimension(),1.0);
            safeZones[0] = new SafeZone(factors, powers);
        }
        else {
            double a = 0;
            double b = 0;
            safeZones = new SafeZone[1];
            for (int i = 0; i < refrencePoint.getDimension(); i++) {
                factors.set(i,(double) refrencePoint.getDimension());
                powers.set(i,1.0);
                a = a + Math.pow(refrencePoint.get(i), 2);
            }
            b = Math.sqrt(a);
            factors.set(refrencePoint.getDimension(),0.0 - Math.sqrt(threshold)*(a/b));
            powers.set(refrencePoint.getDimension(),1.0);
            safeZones[0] = new SafeZone(factors, powers);
        }

    }

    @Override
    double F(IVector<Integer, Double> lsv) {
        return lsv.get(0)*lsv.get(0)+lsv.get(1)*lsv.get(1);
    }
}
