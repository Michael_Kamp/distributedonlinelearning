package eu.ferari.core.monitoring.syncOps;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;
/**
 * Created by mofu on 30/08/16.
 */
public class SafeZone {
	protected RealValuedVector factors;
    protected RealValuedVector powers;

    public SafeZone(RealValuedVector factors, RealValuedVector powers) {
        this.factors = new RealValuedVector(factors.getDimension());
        this.factors.initialise(factors);
        this.powers = new RealValuedVector(powers.getDimension());
        this.powers.initialise(powers);
    }


    /**
     * This method returns true if the refrencePoint lies in the same side of the
     * safe zone where driftVector lies.
     * If result1 and result2 have the same operator, that means the refrencePoint and the driftVector
     * lie in the same side of the safe zone. In this case there is no Local Violation.
     */
    public boolean hasSafeZoneViolation(IVector<Integer,Double> estimate, RealValuedVector drift, boolean equalityInAboveThresholdRegion) {
        float result1 = 0;
        for (int i = 0; i < estimate.getDimension(); i++){
            result1 += factors.get(i) * Math.pow(drift.get(i), powers.get(i));
        }
        result1 += factors.get(estimate.getDimension());

        float result2 = 0;
        for (int i = 0; i < estimate.getDimension(); i++){
            result2 += factors.get(i) * Math.pow(estimate.get(i), powers.get(i));
        }
        result2 += factors.get(estimate.getDimension());

        // if there is local violation return true
        if (equalityInAboveThresholdRegion){
            return((result1 < 0 && result2 >= 0) || (result1 >= 0 && result2 < 0));
        }
        else{
            return((result1 <= 0 && result2 > 0) || (result1 > 0 && result2 <= 0));
        }
    }
}
