package eu.ferari.core.monitoring.states;


import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * Created by mofu on 15/10/15.
 */
public class IdentityState extends MonitoringState {
    public IdentityState(int historySize){
        super(1); //No need for more just pass the value.
        history.add(0.0);
    }

    @Override
    public IVector<Integer, Double> getLocalVariable() {
        RealValuedVector ret =new RealValuedVector(historySize);
        for(int i = historySize-1; i>=0; --i){
            ret.set(i,history.get(i));
        }
        return ret;
    }

}