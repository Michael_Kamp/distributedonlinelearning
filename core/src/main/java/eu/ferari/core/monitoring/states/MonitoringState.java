package eu.ferari.core.monitoring.states;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.IComEffState;
import eu.ferari.core.interfaces.IVector;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.io.Serializable;
/**
 * Created by mofu on 30/08/16.
 */
abstract public class MonitoringState implements IComEffState<Integer, Double>, Serializable {
    final int historySize;
	CircularFifoQueue<Double> history;
    public MonitoringState(int historySize){
        this.historySize = historySize;
		history = new CircularFifoQueue<Double>(historySize);
    }
	public void processInput(DataTuple data) {
		//TODO get variable input data!
		history.add(data.getInteger(3).doubleValue());
	}
	@Override
	public void setLocalVariable(IVector<Integer, Double> iVector) {
		//No need for functionality. In monitoring the ComEffState only uses observed data.
	}
}
