package eu.ferari.core.monitoring.syncOps;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * Created by mofu on 30/08/16.
 */
public class VarianceSafeZoneCheck extends  SafeZoneCheck {

    public VarianceSafeZoneCheck(float threshold, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold) {
        super(threshold, 2, equalityInAboveThresholdRegion, defLSVValue, dynamicThreshold);
    }

    @Override
    protected void computeSafeZones() {

        safeZones = new SafeZone[1];
        RealValuedVector factors = new RealValuedVector(refrencePoint.getDimension() + 1);
        RealValuedVector powers = new RealValuedVector(refrencePoint.getDimension() + 1);
        if (F(refrencePoint) > threshold) {
            factors.set(0,1.0);
            factors.set(1,-1.0);
            factors.set(2,0.0-threshold);
            powers.set(0,1.0);
            powers.set(1,2.0);
            powers.set(2,1.0);

        }
        else {
            factors.set(0,1.0);
            factors.set(1,0.0);
            factors.set(2,0.0-threshold);
            powers.set(0,1.0);
            powers.set(1,1.0);
            powers.set(0,1.0);

        }
        safeZones[0] = new SafeZone(factors, powers);
    }

    double F(IVector<Integer, Double> lsv) {
        return lsv.get(0)-lsv.get(1)*lsv.get(1);
    }
}
