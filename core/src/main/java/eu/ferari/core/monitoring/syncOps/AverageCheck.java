package eu.ferari.core.monitoring.syncOps;

import eu.ferari.core.interfaces.IVector;

/**
 * Created by mofu on 30/08/16.
 */
public class AverageCheck extends BallCheck{
	public AverageCheck(float threshold, int estimateLength, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold){
        super(threshold,estimateLength,equalityInAboveThresholdRegion,defLSVValue,dynamicThreshold);
    }
    /**
     *
     * @param lsv
     * @return the squared norm of the projection to the first components of  {@code lsv}.
     */
    @Override
    double F(IVector<Integer, Double> lsv) {
        return (lsv.get(0) * lsv.get(0)) + (lsv.get(1)*lsv.get(1));
    }
}
