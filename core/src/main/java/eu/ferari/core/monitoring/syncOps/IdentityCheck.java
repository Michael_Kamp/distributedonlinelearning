package eu.ferari.core.monitoring.syncOps;

import eu.ferari.core.interfaces.IVector;

/**
 * Created by mofu on 30/08/16.
 */
	//TODO transform into sum check. that sums up all entries of lsv.
    //This could be used for identityCheck and SphereSafeZoneCheck.
public class IdentityCheck extends BallCheck {
    public IdentityCheck(float threshold, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold){
        super(threshold,1,equalityInAboveThresholdRegion,defLSVValue,dynamicThreshold);
    }
    @Override
    double F(IVector<Integer, Double> lsv) {
        return lsv.get(0);
    }
}
