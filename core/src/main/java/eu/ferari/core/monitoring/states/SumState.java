package eu.ferari.core.monitoring.states;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * Created by mofu on 30/08/16.
 */
public class SumState extends MonitoringState {
    private int counter;
    public SumState(int historySize){
        super(historySize);
    }

    @Override
    public IVector<Integer, Double> getLocalVariable() {
        Double sum =0.0;
        for(Double d : history){
            sum +=d;
        }
        IVector<Integer,Double> ret = new RealValuedVector(1);
        ret.set(0,sum);
        return ret;
    }

}
