package eu.ferari.core.monitoring.syncOps;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * Created by mofu on 30/08/16.
 */
public abstract class BallCheck extends ConditionCheck {
	protected int dimensional;
    public BallCheck(float threshold, int estimateLength, boolean equalityInAboveThresholdRegion, float defLSVValue, boolean dynamicThreshold){
        super(threshold,estimateLength,equalityInAboveThresholdRegion,defLSVValue,dynamicThreshold);
        dimensional=estimateLength;
    }
    @Override
    public boolean checkLocalCondition(IVector<Integer, Double> lsv) {
        return hasBallViolation(lsv);
    }
    boolean hasBallViolation(IVector<Integer,Double> lsv){
        int range = 21;     //grid range
        int Counter = 0;
        int X;
        boolean violation = false;
        int limit = (int) Math.pow(range, dimensional);

        double max = F(refrencePoint);

        double radius = radius();
        IVector <Integer, Double> center = center();
        while( Counter < limit )
        {
            X = Counter;

            double dist = 0.0;
            RealValuedVector point =  new RealValuedVector(dimensional);
            for(int i = 0 ; i < dimensional; i++){
                point.set(i,center.get(i) - (float)radius + 2 * (float)radius * (X % range) / (range - 1));
                dist = dist + ((point.get(i) - center.get(i)) * (point.get(i) - center.get(i)));
                X /= range;
            }

            if(dist <= radius * radius) {
                double currValue = F(point);
                if(equalityInAboveThresholdRegion){
                    if((max < threshold && currValue >= threshold) || (max >= threshold && currValue < threshold)){
                        violation = true;
                        break;
                    }
                }
                else{
                    if((max <= threshold && currValue > threshold) || (max > threshold && currValue <= threshold)){
                        violation = true;
                        break;
                    }
                }
            }
            Counter ++;
        }
        return violation;
    }

    /**
    @Override
    public IVector<Integer, Double> transformVarToLSV(IVector<Integer, Double> v) {
        return null;
    }

    @Override
    public IVector<Integer, Double> transformLSVToVar(IVector<Integer, Double> lsv) {
        return null;
    }
    **/
    /**
     *C = (refrencePoint + driftVector) / 2
     */
    public IVector<Integer,Double> center(){
        RealValuedVector center = new RealValuedVector(driftVector.getDimension());
        center.initialise(driftVector);
        center.add(refrencePoint);
        center.scalarMultiply(.5);
        return center;
    }


    /**
     * r = |(refrencePoint - driftVector)| / 2
     */
    public double radius(){
        double radius;
        double sqrRadius = 0;
        RealValuedVector tmp = new RealValuedVector(refrencePoint.getDimension());
        tmp.initialise(refrencePoint);
        tmp.subtract(driftVector);
        tmp.scalarMultiply(.5);
        for(int i = 0; i < dimensional; i++) {
            sqrRadius = sqrRadius + (tmp.get(i) * tmp.get(i));
        }
        radius = Math.sqrt(sqrRadius);
        return radius;
    }
}
