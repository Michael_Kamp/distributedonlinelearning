package eu.ferari.core.monitoring.states;

import eu.ferari.core.interfaces.IVector;
import eu.ferari.core.utils.RealValuedVector;

/**
 * Created by mofu on 30/08/16.
 */
public class VarianceState extends MonitoringState{
    public VarianceState(int historySize){
        super(historySize);
    }

    @Override
    public IVector<Integer, Double> getLocalVariable() {
        double sum=0.0;
        double sumOfSquares=0.0;
		//TODO numerical unstable!
        for(Double d:history){
            sum +=d;
            sumOfSquares += d*d;
        }
        RealValuedVector ret =new RealValuedVector(2);
        ret.set(0,sumOfSquares);
        ret.set(1,sum);
        return ret;
    }

}
