package com.ibm.ferari;

import java.io.Serializable;

public class FerariEvent implements Serializable {

	private static final long serialVersionUID = -1343333749905358415L;
	
	public Serializable getEvent() {
		return event;
	}
	public void setEvent(Serializable event) {
		this.event = event;
	}
	public String getSource() {
		return source;
	}
	
	public String toString() {
		return "From " + source + ", data: " + event;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	private Serializable event;
	private String source;
	
	

}
