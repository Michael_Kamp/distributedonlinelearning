package com.ibm.ferari.util;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Logger {

	private String _name;
	private RandomAccessFile _f;

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS dd-MM-yyyy");

	static {
		File dir = new File("logs");
		if (!dir.exists()) {
			dir.mkdir();
		}
	}

	private static Map<String, Logger> loggers = new ConcurrentHashMap<>();

	private Logger(String name) {
		_name = name;
		try {
			_f = new RandomAccessFile(new File("logs" + File.separator + _name + ".log"), "rw");
			_f.seek(0);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public synchronized void info(String msg) {
		logMsg("INF", msg, _f);
	}

	public synchronized void debug(String msg) {
		logMsg("DBG", msg, _f);
	}
	
	public synchronized void warn(String msg) {
		logMsg("WRN", msg, _f);
	}
	
	public synchronized void error(String msg) {
		logMsg("WRN", msg, _f);
	}

	public synchronized void error(String msg, Throwable e) {
		if (e != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintWriter w = new PrintWriter(baos);
			e.printStackTrace(w);
			w.flush();
			w.close();
			msg += "\n" + new String(baos.toByteArray());
		}
		logMsg("ERR", msg, _f);
	}

	private static void logMsg(String level, String msg, RandomAccessFile f) {
		String threadName = Thread.currentThread().getName();
		StackTraceElement ste = Thread.currentThread().getStackTrace()[3];
		String methodSig = ste.getClassName() + "." + ste.getMethodName() + "():" + ste.getLineNumber();
		StringBuilder sb = new StringBuilder();
		sb.append(dateFormat.format(new Date())).append(" [").append(level).append("] (").append(threadName).append(") ")
				.append(methodSig).append(" ").append(msg).append("\n");
		try {
			f.write(sb.toString().getBytes());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static Logger createLogger(String name) {
		synchronized (Logger.class) {
			if (!loggers.containsKey(name)) {
				loggers.put(name, new Logger(name));
			}
		}
		return loggers.get(name);
	}

	public static void main(String[] args) {
		Logger l = new Logger("test");
		l.info("bla bla");
		l.info("bla bla");
		l.info("bla bla");
		try {
			fail1();
		} catch (Exception e) {
			l.error("Caught failure", e);
		}
		l.warn("bla bla");
	}
	
	private static void fail1() {
		fail2();
	}
	
	private static void fail2() {
		try {
			fail3();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private static void fail3() {
		throw new RuntimeException("Generic Failure");
	}

}
