package com.ibm.ferari.storm;

import java.util.LinkedList;
import java.util.List;

import org.apache.thrift7.TException;

import com.ibm.ferari.FerariConstants;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.TopologySummary;
import backtype.storm.generated.Nimbus.Client;
import backtype.storm.utils.NimbusClient;

public class StormUtilityHelper {

	private Client _cl;
	
	public List<TopologySummary> listTopologies(LocalCluster lc) {
		return new LinkedList<TopologySummary>(lc.getClusterInfo().get_topologies());
	}

	public List<TopologySummary> listTopologies(String nimbusHost, int nimbusPort) {
		try {
			if (_cl == null) {
				Config conf = new Config();
				conf.put(Config.NIMBUS_HOST, nimbusHost);
				conf.put(Config.NIMBUS_THRIFT_PORT, nimbusPort);
				conf.setDebug(true);
				conf.put("storm.thrift.transport", FerariConstants.NIMBUS_THRIFT_DEF_PROTOCOL);
				NimbusClient client = new NimbusClient(conf, nimbusHost, nimbusPort);
				_cl = client.getClient();
			}
			
			return new LinkedList<TopologySummary>(_cl.getClusterInfo().get_topologies());
			
		} catch (TException e) {
			throw new RuntimeException("Failed obtaining topology information", e);
		}
	}
	
}
