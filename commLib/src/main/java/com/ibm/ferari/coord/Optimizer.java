package com.ibm.ferari.coord;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Topic;
import javax.management.ObjectName;

import com.ibm.ferari.FerariConstants;
import com.ibm.ferari.FerariMessage;
import com.ibm.ferari.FerariMessage.MessageType;
import com.ibm.ferari.exceptions.FerariStartupException;
import com.ibm.ferari.OptimizerRequest;
import com.ibm.ferari.StormKillCommand;
import com.ibm.ferari.StormStartCommand;
import com.ibm.ferari.StormStartResult;
import com.ibm.ferari.TopologyPlan;

import backtype.storm.Config;
import backtype.storm.generated.StormTopology;

public class Optimizer extends Coordinator {

	private MessageProducer _sitesUpdater;
	private volatile Map<String, List<String>> _topologyGraph;
	private volatile Map<String, Serializable> _customConfig;
	private final static Pattern queuePattern = Pattern.compile(".+destinationName=TO_COORD_DATA.(.+?),clientId=.+");

	private LinkedBlockingQueue<StormStartResult> _topologyResults = new LinkedBlockingQueue<StormStartResult>();

	/****
	 * Constructs an {@link Optimizer} instance
	 * 
	 * @param siteId
	 *            The id of the site
	 * @param dir
	 *            The directory to store ActiveMQ files in
	 * @param port
	 *            The port to bind to
	 * @param eventConsumer
	 *            Defines what to do with incoming messages from sites
	 */
	public Optimizer(String siteId, String dir, int port, EventConsumer eventConsumer) {
		super(siteId, dir, port, eventConsumer, true);
	}
	
	private void sendMembership2Sites(LinkedList<String> siteList) {
		try {
			Message msg = _selfSess.createObjectMessage(siteList);
			_sitesUpdater.send(msg);
		} catch (JMSException e) {
			_logger.error("Failed updating sites for membership", e);
		}
	}

/*	private void printSubscriptions() {
		for (Entry<String, List<Subscription>> e : _subscriptions.entrySet()) {
			System.out.println(e.getKey() + " " + e.getValue().toString());
		}
	}*/

	/***
	 * Publishes and saves (in-memory) a topology.
	 * 
	 * @param topologyGraph
	 *            the topology graph defined in the following way: for each
	 *            outgoing edge e=(v,u) in the topology graph, the list of the
	 *            key v should contain u.
	 */
	public void setTopologyGraph(Map<String, List<String>> topologyGraph, Map<String, Serializable> customConfig) {
		_logger.info(topologyGraph.toString());
		_topologyGraph = topologyGraph;
		_customConfig = customConfig;
		sendMessage(new TopologyPlan(null, _topologyGraph, _customConfig));
/*		for (List<Subscription> subs : _subscriptions.values()) {
			for (Subscription sub : subs) {
				sub.sendMessage();
			}
		}*/
	}

	/***
	 * @return The sites that are connected to the optimizer
	 */
	public Set<String> getActiveSites() {
		//TODO: implement this 
		return new TreeSet<String>();
	}

	/***
	 * Broadcasts msg to all connected sites
	 * 
	 * @param msg
	 */
	public void broadcast(Serializable msg) {
		sendMessage(new OptimizerRequest(null, msg));
	}

	/***
	 * Return true, if site was connected during the transmission and false
	 * otherwise
	 */
	public boolean sendRequestToSite(Serializable msg, String site) {
/*		List<Subscription> subs = _subscriptions.get(site);
		if (subs == null) {
			return false;
		}
		for (Subscription sub : subs) {
			sub.sendMessage();
		}*/
		sendMessage(new OptimizerRequest(site, msg));
		return true;
	}

/*	private void maintainSubscriptions() {
		Set<String> activeSubs = new HashSet<>();
		try {
			for (ObjectName on : _broker.getAdminView().getQueueProducers()) {
				Matcher m = queuePattern.matcher(on.toString());
				if (m.matches()) {
					System.out.println("learned " + m.group(1));
					activeSubs.add(m.group(1));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		activeSubs.stream().filter(s -> !_subscriptions.keySet().contains(s)).forEach(s -> {
			System.err.println("Created subscription for optimizer (line 156)");
			createSub(s);
		});
	}*/

	@Override
	protected void handleNonEventMessage(FerariMessage ferrMsg) {
		_logger.debug("Got message of type " + ferrMsg.getMsgType().name());

		if (ferrMsg.getMsgType() == MessageType.TopologyPlan) {
			_logger.warn("Cannot handle message of type " + ferrMsg.getMsgType().name());
			return;
		}

/*		if (ferrMsg.getMsgType() == MessageType.OptimizerRequest) {
			String site = (String) ferrMsg.getUserMsg();
			if (_subscriptions.containsKey(site)) {
				return;
			}
			System.err.println("Created subscription for optimizer (line 175)");
			createSub(site);
			return;
		}*/

/*		if (ferrMsg.getMsgType() == MessageType.TopologyRequest) {
			String sender = ferrMsg.getSenderId();
			List<Subscription> subs = _subscriptions.get(sender);
			if (subs != null) {
				for (Subscription sub : subs) {
					sub.sendMessage(new TopologyPlan(sender, _topologyGraph, _customConfig));
				}
			} else {
				_logger.warn("Received a message from " + sender + " but subscription object wasn't found");
			}
		}*/

		if (ferrMsg.getMsgType() == MessageType.StormStartResult) {
			String sender = ferrMsg.getSenderId();
			if (ferrMsg.getUserMsg() instanceof Throwable) {
				Throwable e = (Throwable) ferrMsg.getUserMsg();
				_logger.error(sender + " failed starting topology", e);
				_topologyResults.offer(new StormStartResult(sender, e));
			} else {
				String topologyName = (String) ferrMsg.getUserMsg();
				_logger.info("Got " + MessageType.StormStartResult.name() + " from " + sender + ":" + topologyName);
				_topologyResults.offer(new StormStartResult(sender, topologyName));
			}
		}
	}

	public void killTopology(String site, String topologyId) {
		StormKillCommand skc = new StormKillCommand(site, topologyId);
		_logger.debug("Sending " + skc.getMsgType() + " to " + site + " for topology " + topologyId);

		sendMessage(skc);
	}

	public String startTopology(String site, Config conf, StormTopology topology) throws FerariStartupException {
		StormStartCommand ssc = new StormStartCommand(site, conf, topology);
		_logger.debug("Sending " + site + " a " + ssc.getMsgType() + " message");
/*		List<Subscription> subs = _subscriptions.get(site);
		if (subs == null) {
			_logger.warn(
					"Oops, can't send a topology start request to site " + site + " because I don't know such a site");
			return null;
		}*/
		sendMessage(ssc);
		return waitForResult(FerariConstants.STORM_TOP_STARTUP_TIMELIMIT_MILLIS, site);
	}

	private String waitForResult(long timeoutMillis, String site) throws FerariStartupException {
		long start = System.currentTimeMillis();
		while (true) {
			long elapsed = System.currentTimeMillis() - start;
			if (elapsed > timeoutMillis) {
				fireTimeoutExpiredOnStormStartRequest(site, timeoutMillis);
			}

			try {
				StormStartResult ssr = _topologyResults.poll(timeoutMillis, TimeUnit.MILLISECONDS);
				if (ssr == null) {
					fireTimeoutExpiredOnStormStartRequest(site, timeoutMillis);
				}

				// It's a request for a different site, ignore it...
				if (!ssr.getSenderId().equals(site)) {
					// put back into the tail of the queue
					_topologyResults.offer(ssr);
					continue;
				}

				if (ssr.getUserMsg() instanceof Throwable) {
					throw new FerariStartupException("Failed starting topology on site " + ssr.getSenderId(),
							(Throwable) ssr.getUserMsg());
				} else {
					return (String) ssr.getUserMsg();
				}
			} catch (InterruptedException e) {
			}
		}
	}

	private void fireTimeoutExpiredOnStormStartRequest(String site, long timeout) throws FerariStartupException {
		throw new FerariStartupException(
				"Failed starting topology on site " + site + ", timeout(" + timeout + " ms) expired");
	}

}
