package com.ibm.ferari.coord;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.command.ActiveMQMessage;
import org.apache.activemq.command.ConnectionInfo;
import org.apache.activemq.command.RemoveInfo;

import com.ibm.ferari.FerariConstants;
import com.ibm.ferari.FerariEvent;
import com.ibm.ferari.FerariMessage;
import com.ibm.ferari.FerariMessage.MessageType;
import com.ibm.ferari.util.Logger;

public class Coordinator implements MessageListener {

	private static final String MQ_DATA_DIR = "activeMQdata";

	private Connection _selfConn;
	protected Session _selfSess;
	protected MessageProducer _msgProducer;

	private Map<String, String> _connectionIdss2Sites = new HashMap<String, String>();
	//protected final Map<String, List<Subscription>> _subscriptions = new ConcurrentHashMap<String, List<Subscription>>();

	protected Logger _logger;
	protected BrokerService _broker;
	private boolean _manualAck = false;
	protected String _siteId;
	private String _dirName;
	private int _port;
	private volatile EventConsumer _eventConsumer;

	/****
	 * A coordinator of a site
	 * 
	 * @param siteId
	 *            The id of the site
	 * @param dir
	 *            The directory to store ActiveMQ files in
	 * @param port
	 *            The port to bind to
	 * @param eventConsumer
	 *            Defines what to do when a message is received
	 */

	public Coordinator(String siteId, String dir, int port, EventConsumer eventConsumer) {
		this(siteId, dir, port, eventConsumer, false);
	}

	Coordinator(String siteId, String dir, int port, EventConsumer eventConsumer, boolean useJMX) {
		_logger = Logger.createLogger("coordinator-" + siteId);
		_port = port;
		_siteId = siteId;
		_dirName = dir;
		_eventConsumer = eventConsumer;
		try {
			createBroker(useJMX);
		} catch (Exception e) {
			_logger.error("Failed creating broker", e);
		}
	}

	private void createBroker(boolean useJMX) throws Exception {
		_broker = new BrokerService();
		if (!useJMX) {
			_broker.setUseJmx(false);
		}
		_broker.setBrokerId(_siteId);
		_broker.setDataDirectory(_dirName + File.separator + MQ_DATA_DIR);
		_broker.setBrokerName(_siteId);
		_logger.info("Creating broker " + _siteId + " listening on " + _port);
		_broker.addConnector("tcp://" + "0.0.0.0" + ":" + _port + "?daemon=true");
	}

	public void start() {
		_logger.info("Starting coordinator " + _siteId);
		try {
			_broker.start();
		} catch (Exception e) {
			_logger.error("Failed starting coordinator", e);
			return;
		}
		_logger.info("Coordinator " + _siteId + " started");

		connectToSelf();
	}

/*	private void subscribe2MembershipTopic() {
		if (_siteId.equals(FerariConstants.OPTIMIZER)) {
			return;
		}
		try {
			Topic top = _selfSess.createTopic(FerariConstants.MEMBERSHIP_TOPIC_NAME);
			MessageConsumer cons = _selfSess.createConsumer(top);
			cons.setMessageListener(new MessageListener() {

				@SuppressWarnings("unchecked")
				@Override
				public void onMessage(Message message) {
					ObjectMessage objMsg = (ObjectMessage) message;
					LinkedList<String> membership;
					try {
						membership = (LinkedList<String>) objMsg.getObject();
					} catch (JMSException e) {
						_logger.error("Failed obtaining objectMessage for membership subscriber", e);
						return;
					}
					membership.stream().filter(s -> !s.equals(_siteId)).forEach(s -> {
						createSub(s);
					});
				}
			});
		} catch (JMSException e) {
			_logger.error("Failed subscribing to membership topic", e);
		}
	}*/

	private void connectToSelf() {
		try {
			_selfConn = new ActiveMQConnectionFactory("tcp://localhost:" + _port).createConnection();
		} catch (JMSException e) {
			// System.err.println("Unable to establish connection to self broker
			// instance");
			_logger.error("Unable to establish connection to self broker instance", e);
			return;
		}

		try {
			_selfSess = _selfConn.createSession(false, Session.AUTO_ACKNOWLEDGE);

			_selfConn.start();

		} catch (JMSException e) {
			_logger.error("Unable to establish session to self broker instance", e);
			return;
		}

		try {
			Topic fromSites = _selfSess.createTopic(FerariConstants.TO_COORDINATOR_DATA_TOPIC_NAME);
			MessageConsumer cons = _selfSess.createConsumer(fromSites);
			cons.setMessageListener(this);
			
			Topic toSites = _selfSess.createTopic(FerariConstants.FROM_COORDINATOR_DATA_TOPIC_NAME);
			_msgProducer =  _selfSess.createProducer(toSites);
			
/*			Destination destinationAdvisory = _selfSess.createTopic("ActiveMQ.Advisory..>");
			MessageConsumer consumerAdvisory = _selfSess.createConsumer(destinationAdvisory);

			consumerAdvisory.setMessageListener(new MessageListener() {

				public void onMessage(Message message) {
					if (message instanceof ActiveMQMessage) {
						Object command = ((ActiveMQMessage) message).getDataStructure();
						try {
							if (command instanceof ConnectionInfo) {
								handleNewClientConnection(command);
							}

							if (command instanceof RemoveInfo) {
								handleClientDisconnect(command);

							}
						} catch (JMSException e) {
							_logger.error("Failed handing client (dis)connection", e);
						}
					}
				}
			});*/

		} catch (JMSException e) {
			_logger.error("Failed setting channel advisory", e);
		}
	}

	public void stop() {
		try {
			_selfConn.stop();
			_selfConn.close();
			_broker.stop();
		} catch (Exception e) {
			e.printStackTrace();
			_logger.error("Failed stopping coordinator", e);
		}

	}

/*	private void handleClientDisconnect(Object command) {
		String connId = ((RemoveInfo) command).getObjectId().toString();

		if (connId.startsWith(FerariConstants.ANONYMOUS_CLIENT_ID)) {
			return;
		}

		if (!_connectionIdss2Sites.containsKey(connId)) {
			_logger.warn("Got disconnected (" + connId + ") but couldn't associate connectionId to site");
			return;
		}

		String siteId = _connectionIdss2Sites.get(connId).split("\\.")[0];
		_logger.info(siteId + " disconnected (" + connId + "), site: " + siteId);

		if (connId.startsWith(FerariConstants.ANONYMOUS_CLIENT_ID)) {
			return;
		}

		_connectionIdss2Sites.remove(connId);
		List<Subscription> subs = _subscriptions.get(siteId);
		Subscription sub = null;
		for (Subscription s : subs) {
			if (connId.equals(s.connId)) {
				sub = s;
				break;
			}
		}

		if (sub != null) {
			System.err.println("Removing subscription to " + siteId);
			_subscriptions.remove(siteId);
			sub.close();
		} else {
			_logger.warn("Attempted to close subscription for " + siteId + " but it wasn't found");
		}
	}
*/
/*	void createSub(String site) {
		createSub(site, null);
	}

	synchronized void createSub(String site, String connId) {
		try {
			System.out.println("Creating subscription for " + site + "...");
			_logger.warn("Creating subscription for " + site + "...");

			if (!_subscriptions.containsKey(site)) {
				_subscriptions.put(site, new Vector<Subscription>());
				_subscriptions.get(site).add(new Subscription(site, connId, this, _selfSess, _logger, _subscriptions));
			}
		} catch (Exception e) {
			_logger.error("Failed creating subscription for " + site);
		}
	}*/
	
	void sendMessage(FerariMessage msg) {
		_logger.debug("Sending " + msg);
		ObjectMessage objMsg;
		try {
			objMsg = _selfSess.createObjectMessage();
			objMsg.setObject(msg);
			_msgProducer.send(objMsg);
		} catch (JMSException e) {
			_logger.error("Failed sending message " + msg, e);
		}
	}

	private void handleNewClientConnection(Object command) throws JMSException {
		String connId = ((ConnectionInfo) command).getConnectionId().getValue();
		String siteId = ((ConnectionInfo) command).getClientId();

		if (connId.startsWith(FerariConstants.ANONYMOUS_CLIENT_ID)) {
			return;
		}

		siteId = ((ConnectionInfo) command).getClientId().split("\\.")[0];

		String ip = ((ConnectionInfo) command).getClientIp();
		_logger.info(siteId + " connected (" + connId + ") from " + ip);
		_connectionIdss2Sites.put(connId, siteId);
		/*createSub(siteId, connId);*/
	}

	public void onMessage(Message message) {
		handleMsgFromSite(message);
	}

	private void handleMsgFromSite(Message msg) {
		if (msg instanceof ObjectMessage) {
			try {
				FerariMessage ferrMsg = ((FerariMessage) (((ObjectMessage) msg).getObject()));
				_logger.debug("Got message: " + ferrMsg);
				if (ferrMsg.getMsgType() == MessageType.Event) {
					handleEventMessage(ferrMsg);
				} else {
					handleNonEventMessage(ferrMsg);
				}

				ackMsg(msg);
			} catch (JMSException e) {
				_logger.error("Failed receiving message", e);
			} catch (ClassCastException e) {
				_logger.error("Failed casting message from type " + msg.getClass().getName(), e);
				ackMsg(msg);
			}
		} else {
			// System.err.println("Message isn't an object message");
			_logger.warn("Got a message of a non-ObjectMessage type (" + msg.getClass().getName() + ")");
			ackMsg(msg);
		}
	}

	private void handleEventMessage(FerariMessage ferrMsg) {
		_logger.debug(ferrMsg.toString());
		String sender = ferrMsg.getSenderId();

		FerariEvent event = new FerariEvent();
		event.setEvent(ferrMsg.getUserMsg());
		event.setSource(sender);

		_eventConsumer.consumeEvent(event);
	}

	protected void handleNonEventMessage(FerariMessage ferrMsg) {
		String msgType = ferrMsg.getMsgType() != null ? ferrMsg.getMsgType().name() : null;
		_logger.warn("Got message of type " + msgType + " but don't know how to handle it");
		_logger.debug(ferrMsg.getUserMsg().toString());
	}

	private void ackMsg(Message msg) {
		if (!_manualAck) {
			return;
		}
		try {
			msg.acknowledge();
		} catch (JMSException e) {
			_logger.error("Failed acking message", e);
		} finally {
			_logger.debug("Ack message " + msg);
		}
	}

	static class Subscription {

		Subscription(String siteId, String connId, MessageListener consumerMessageLsnr, Session selfSess, Logger logger,
				Map<String, List<Subscription>> subs) throws JMSException {
			this.siteId = siteId;
			this.connId = connId;
			this._logger = logger;
			this._subs = subs;
			this._selfSession = selfSess;
			// System.out.println("Created new subscription for " + siteId);
			producer = _selfSession.createProducer(
					_selfSession.createQueue(FerariConstants.FROM_COORDINATOR_DATA_TOPIC_NAME + "." + siteId));
			consumer = _selfSession.createConsumer(
					_selfSession.createQueue(FerariConstants.TO_COORDINATOR_DATA_TOPIC_NAME + "." + siteId));
			consumer.setMessageListener(consumerMessageLsnr);
		}

		private Map<String, List<Subscription>> _subs;
		private MessageConsumer consumer;
		private MessageProducer producer;
		private String siteId;
		private String connId;
		private Logger _logger;
		private Session _selfSession;

		void sendMessage(FerariMessage msg) {
			_logger.debug("Sending " + msg);
			ObjectMessage objMsg;
			try {
				objMsg = _selfSession.createObjectMessage();
				objMsg.setObject(msg);
				producer.send(objMsg);
			} catch (JMSException e) {
				_logger.error("Failed sending message " + msg, e);
			}
		}

		String getSiteId() {
			return siteId;
		}

		private void close() {
			System.err.println("Closing subscription " + siteId);
			try {
				_subs.remove(siteId);
				consumer.setMessageListener(null);
				consumer.close();
				producer.close();
			} catch (JMSException e) {
				_logger.error("Failed closing subscription " + siteId, e);
			}
		}
	}
}
