package com.ibm.ferari;

import java.io.Serializable;

import backtype.storm.generated.ClusterSummary;

public interface StormMonitoringFilter {

	public Serializable filterClusterSummary(ClusterSummary clusterSummary);
	
}
