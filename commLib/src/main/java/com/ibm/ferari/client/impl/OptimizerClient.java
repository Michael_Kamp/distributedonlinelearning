package com.ibm.ferari.client.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


import com.ibm.ferari.FerariConstants;
import com.ibm.ferari.FerariMessage;
import com.ibm.ferari.FerariMessage.MessageType;
import com.ibm.ferari.OptimizerRequest;
import com.ibm.ferari.TopologyPlan;
import com.ibm.ferari.exceptions.FerariStartupException;

public class OptimizerClient extends SiteSender {

	private TopologyChangeListener _topoChangeLsnr;
	private OptimizerMessagelistener _optMsgLsnr;

	OptimizerClient(TopologyChangeListener topoChangeLsnr, OptimizerMessagelistener optMsgLsnr,String id, String[] brokers,
			int maxRetransTimeThresh, com.ibm.ferari.util.Logger logger) throws FerariStartupException {
		super(id, FerariConstants.OPTIMIZER, brokers, maxRetransTimeThresh, logger);
		_topoChangeLsnr = topoChangeLsnr;
		_optMsgLsnr = optMsgLsnr;
		sendMsg(new OptimizerRequest(FerariConstants.OPTIMIZER, id));
	}

	@Override
	protected void handleMsg(FerariMessage msg) {
		_logger.info("Received message of type " + msg.getMsgType().name());
		if (msg.getMsgType() == MessageType.TopologyPlan) {
			try {
				Map<String, List<String>> graph = ((TopologyPlan) msg).getTopologyGraph();
				Map<String, Serializable> customConfig = ((TopologyPlan) msg).getCustomConfig();
				if (_topoChangeLsnr != null) {
					_topoChangeLsnr.topologyChange(graph, customConfig);
				}
			} catch (ClassCastException e) {
				_logger.error("Message is declared to be of type " + msg.getMsgType() + " but object is of type "
						+ msg.getClass().getName());
			}
		} else if (msg.getMsgType() == MessageType.OptimizerRequest || msg.getMsgType() == MessageType.StormStartCmd || msg.getMsgType() == MessageType.StormKillCmd) {
			_optMsgLsnr.onMessage(msg);
		} else {
			_logger.info("Can't handle message of type " + msg.getMsgType().name());
		}
	}

	static interface TopologyChangeListener {

		void topologyChange(Map<String, List<String>> topologyGraph, Map<String,Serializable> customConfig);

	}
	
	static interface OptimizerMessagelistener {

		void onMessage(FerariMessage msg);

	}

}
