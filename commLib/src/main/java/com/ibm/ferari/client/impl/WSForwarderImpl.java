package com.ibm.ferari.client.impl;


import java.util.concurrent.Semaphore;

import com.ibm.ferari.client.WSForwarder;

import io.socket.client.Ack;
import io.socket.client.IO;

public class WSForwarderImpl implements WSForwarder {

	protected String _uri;
	private io.socket.client.Socket _wss;
	private String _topic;
	private Semaphore _sem = new Semaphore(1);
	
	WSForwarderImpl(String uri, String topic) {
		_uri = uri;
		_topic = topic;
		try {
			_wss = IO.socket(uri);
			_wss.connect();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Override
	public void forward(String event) {
		_wss.emit(_topic, event);
	}


	@Override
	public void close() {
		_wss.disconnect();
	}


	@Override
	public void forwardSync(String event) {
		acquire();
		_wss.emit(_topic, event, new Ack() {
			@Override
			public void call(Object... args) {
				_sem.release();
			}
		});
	}


	private void acquire() {
		while (true) {
			try {
				_sem.acquire();
				return;
			} catch (InterruptedException e) {
			}
		}
	}

}
