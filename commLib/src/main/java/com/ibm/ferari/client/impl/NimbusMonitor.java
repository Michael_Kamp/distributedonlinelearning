package com.ibm.ferari.client.impl;

import org.apache.thrift7.TException;

import com.ibm.ferari.FerariConstants;
import com.ibm.ferari.client.NimbusMonitorConfig;
import com.ibm.ferari.util.Logger;
import com.ibm.ferari.util.Util;

import backtype.storm.Config;
import backtype.storm.generated.ClusterSummary;
import backtype.storm.generated.Nimbus.Client;
import backtype.storm.utils.NimbusClient;

class NimbusMonitor implements Runnable {

	private volatile boolean _alive = true;
	private int _monitorInterval;
	private Logger _logger;
	private String _nimbusHost;
	private int _nimbusPort;
	private NimbusNotifier _nimbusNotifier;
	private Thread _thisThread;

	NimbusMonitor(NimbusMonitorConfig config, NimbusNotifier notifier) {
		_logger = Logger.createLogger("NimbusMonitor" + config.getNimbusHost() + ":" + config.getNimbusPort());
		_nimbusHost = config.getNimbusHost();
		_nimbusPort = config.getNimbusPort();
		_monitorInterval = config.getMonitorInterval();
		_nimbusNotifier = notifier;
		_thisThread = new Thread(this, "NimbusMonitor");
		_thisThread.start();
	}

	private void monitor() throws TException {
		Config conf = new Config();
		conf.put(Config.NIMBUS_HOST, _nimbusHost);
		conf.put(Config.NIMBUS_THRIFT_PORT, _nimbusPort);
		conf.setDebug(true);
		conf.put("storm.thrift.transport", FerariConstants.NIMBUS_THRIFT_DEF_PROTOCOL);
		NimbusClient client = new NimbusClient(conf, _nimbusHost, _nimbusPort);
		Client cl = client.getClient();
		ClusterSummary summary = cl.getClusterInfo();
		_logger.info("Sending state " + summary);
		_nimbusNotifier.emitState(summary);
		client.close();
	}

	public void stop() {
		_alive = false;
		_thisThread.interrupt();
	}
	
	@Override
	public void run() {
		while (_alive) {
			try {
				monitor();
			} catch (TException e) {
				_logger.error("Failed monitoring Nimbus", e);
				_nimbusNotifier.emitError(e);
			} catch (Exception e) {
				_logger.error("Something went wrong, got an unexpected error. Skipped error emission", e);
			}
			Util.sleep(_monitorInterval);
		}
	}

}
