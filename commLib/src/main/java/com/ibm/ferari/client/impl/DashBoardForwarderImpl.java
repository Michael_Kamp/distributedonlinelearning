package com.ibm.ferari.client.impl;

import com.ibm.ferari.client.DashboardForwarder;

public class DashBoardForwarderImpl extends WSForwarderImpl implements DashboardForwarder {

	DashBoardForwarderImpl(String uri, String topic) {
		super(uri, "fraudEvents");
	}

}
