package com.ibm.ferari.client;

public interface WSForwarder {

	public void forward(String event);
	
	public void forwardSync(String event);
	
	public void close();
}
