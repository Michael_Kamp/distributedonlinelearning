package com.ibm.ferari.client;

import java.lang.reflect.Constructor;

import com.ibm.ferari.AddressResolver;
import com.ibm.ferari.FerariConstants;
import com.ibm.ferari.StormMonitoringFilter;
import com.ibm.ferari.exceptions.FerariStartupException;

import backtype.storm.generated.ClusterSummary;

public class FerariClientBuilder {

	private static final FerariClientBuilder instance = new FerariClientBuilder();
	private static Class<?> interSiteCls;
	private static Class<?> intraSiteCls;
	private static Class<?> wsForwarderCls;
	private static Class<?> dashBoardForwarderCls;

	static {
		try {
			interSiteCls = Class.forName("com.ibm.ferari.client.impl.IntersiteClientImpl");
			intraSiteCls = Class.forName("com.ibm.ferari.client.impl.IntraSiteClientImpl");
			wsForwarderCls = Class.forName("com.ibm.ferari.client.impl.WSForwarderImpl");
			dashBoardForwarderCls = Class.forName("com.ibm.ferari.client.impl.DashBoardForwarderImpl");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private AddressResolver _resolver;
	private NimbusMonitorConfig _nimbusConfig = new NimbusMonitorConfig();

	/***
	 * Returns the instance of the builder
	 * @return
	 */
	public static FerariClientBuilder getInstance() {
		return instance;
	}

	/****
	 * Sets the {@link AddressResolver} of the {@link FerariClientBuilder}.
	 * This MUST be invoked before any client instantiations 
	 * @param resolver the {@link AddressResolver} to be used
	 */
	public void setAddressResolver(AddressResolver resolver) {
		_resolver = resolver;
	}

	/***
	 * Sets the monitoring interval (in seconds) of the storm monitoring.
	 * If this method isn't invoked, the value used NIMBUS_MONITOR_INTERVAL in {@link FerariConstants}
	 * @param monitoringInterval The value in seconds
	 */
	public void setStormMonitoringInterval(int monitoringInterval) {
		_nimbusConfig.setMonitorInterval(monitoringInterval);
	}

	/***
	 * Sets the filter to be applied on a {@link ClusterSummary} object prior to be sent to the optimizer
	 * @param monitoringFilter
	 */
	public void setMonitoringFilter(StormMonitoringFilter monitoringFilter) {
		_nimbusConfig.setMonitoringFilter(monitoringFilter);
	}

	/***
	 * The nimbus host (ip or host address).
	 * If not set, defaults to localhost
	 * @param host
	 */
	public void setNimbusHost(String host) {
		_nimbusConfig.setNimbusHost(host);
	}

	/***
	 * The nimbus port to connect to.
	 * If not set, defaults to NIMBUS_DEF_PORT in {@link FerariConstants}
	 * @param port
	 */
	public void setNimbusPort(int port) {
		_nimbusConfig.setNimbusPort(port);
	}

	/***
	 * Creates an intersite client, used for communicating with external sites and the optimizer
	 * @param siteId The id of this site
	 * @return
	 * @throws FerariStartupException 
	 */
	public InterSiteClient createInterSiteClient(String siteId, boolean connect2Optimizer) throws FerariStartupException {
		return createInterSiteClient(siteId, false, connect2Optimizer);
	}
	
	/***
	 * Creates an intersite client that also sends periodical storm information.
	 * 
	 * @param siteId The id of this site
	 * @return
	 * @throws FerariStartupException 
	 */
	public InterSiteClient createMonitoringIntersiteClient(String siteId) throws FerariStartupException {
		return createInterSiteClient(siteId, true);
	}

	private InterSiteClient createInterSiteClient(String siteId, boolean monitorStorm, boolean connect2Optimizer) throws FerariStartupException {
		try {
			if (! connect2Optimizer) {
				Constructor<?> ctor = interSiteCls.getDeclaredConstructor(String.class, AddressResolver.class);
				ctor.setAccessible(true);
				return (InterSiteClient) ctor.newInstance(siteId, _resolver);
			}
			Constructor<?> ctor = interSiteCls.getDeclaredConstructor(String.class, AddressResolver.class,
					NimbusMonitorConfig.class);
			ctor.setAccessible(true);
			if (_resolver == null) {
				System.err.println("_resolver is null");
			}
			return (InterSiteClient) ctor.newInstance(siteId, _resolver, monitorStorm ? _nimbusConfig : null);
		} catch (Exception e) {
			throw new FerariStartupException("Failed instantiating intersite client", e);
		}
	}

	/****
	 * Creates an intrasite client, used for communicating within the site itself.
	 * @param siteId the id of this site, used for determining the Coordinator's endpoint
	 * @return
	 * @throws FerariStartupException
	 */
	public IntraSiteClient createIntraSiteClient(String siteId) throws FerariStartupException {
		try {
			Constructor<?> ctor = intraSiteCls.getDeclaredConstructor(String.class, String[].class);
			ctor.setAccessible(true);
			return (IntraSiteClient) ctor.newInstance(siteId, _resolver.getHostsOfSite(siteId));
		} catch (Exception e) {
			throw new FerariStartupException("Failed instantiating intrasite client", e);
		}
	}
	
	
	public WSForwarder createWSForwarder(String uri, String topic) {
		try {
			Constructor<?> ctor = wsForwarderCls.getDeclaredConstructor(String.class, String.class);
			ctor.setAccessible(true);
			return (WSForwarder) ctor.newInstance(uri, topic);
		} catch (Exception e) {
			throw new FerariStartupException("Failed instantiating WSForwarder", e);
		}
	}
	
	public DashboardForwarder createWSForwarder() {
		try {
			Constructor<?> ctor = dashBoardForwarderCls.getDeclaredConstructor(String.class, String.class);
			ctor.setAccessible(true);
			return (DashboardForwarder) ctor.newInstance(instance._resolver.getDashboardWSURI(), "");
		} catch (Exception e) {
			throw new FerariStartupException("Failed instantiating DashboardForwarder", e);
		}
	}

}
