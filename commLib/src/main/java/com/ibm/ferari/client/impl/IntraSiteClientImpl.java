package com.ibm.ferari.client.impl;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Topic;


import com.ibm.ferari.FerariConstants;
import com.ibm.ferari.FerariMessage;
import com.ibm.ferari.TopologyPlan;
import com.ibm.ferari.FerariMessage.MessageType;
import com.ibm.ferari.TopologyPlan.TopologyConfig;
import com.ibm.ferari.client.IntraSiteClient;
import com.ibm.ferari.client.MessageHandler;
import com.ibm.ferari.client.TopologyRequestHandler;
import com.ibm.ferari.client.impl.ClientUtils.ClientSession;
import com.ibm.ferari.exceptions.FerariStartupException;
import com.ibm.ferari.util.Logger;
import com.ibm.ferari.util.Util;

public class IntraSiteClientImpl implements IntraSiteClient {

	private String _siteId;
	private Logger _logger;
	private volatile ClientSession _session;
	private volatile MessageHandler _optReqHandler;
	private volatile MessageHandler _broadcastReqHandler;
	private volatile TopologyRequestHandler _topReqHandler;
	private Vector<CrossSiteRequest> _crossSiteRequests = new Vector<CrossSiteRequest>();

	private CrossSiteRequestHandler _xsrHandler;
	
	IntraSiteClientImpl(String siteId, String[] hosts) throws FerariStartupException {
		this(siteId, hosts, null);
	}
	
	IntraSiteClientImpl(String siteId, String[] hosts, Logger logger) throws FerariStartupException {
		_siteId = siteId;
		_logger = logger;
		if (_logger == null) {
			_logger = Logger.createLogger(IntraSiteClient.class.getSimpleName() + _siteId + "-" + UUID.randomUUID().toString());
		}
		Util.runAsyncTask(new Runnable() {
			
			@Override
			public void run() {
				while (_session == null) {
					connect2Broker(siteId, hosts);
					Util.sleep(1);
				}
			}
		});
	}
	
	private void connect2Broker(String siteId, String[] hosts) {
		try {
			_session = ClientUtils.connect2SomeBroker(hosts, _logger, siteId);
		} catch (Exception e1) {
			_logger.error("Failed connecting to some broker", e1);
			return;
		}

		try {
			registerMessageHandlers();
		} catch (JMSException e) {
			_session.close();
		}
		_logger.info(_siteId + " intraSiteClient is connected to broker(" + hosts[0] + ")");
	}

	private void registerMessageHandlers() throws JMSException {
		subscribeToMessageType(MessageType.OptimizerRequest, FerariConstants.OPTIMIZER_REQUEST_TOPIC,
				new MsgLsnr() {
					@Override
					public void onMessage(FerariMessage fMsg) {
						if (_optReqHandler != null) {
							_optReqHandler.onMessage(fMsg.getUserMsg());
						}
						
					}
				});

		subscribeToMessageType(MessageType.IntraSiteRequest, FerariConstants.INTRASITE_BROADCAST,
				new MsgLsnr() {
					@Override
					public void onMessage(FerariMessage fMsg) {
						if (_broadcastReqHandler != null) {
							_broadcastReqHandler.onMessage(fMsg.getUserMsg());
						}
					}
				});

		subscribeToMessageType(MessageType.CrossSiteRequest, FerariConstants.CROSS_SITE_REQUEST_TOPIC,
				new MsgLsnr() {
					@Override
					public void onMessage(FerariMessage fMsg) {
						if (_xsrHandler != null) {
							CrossSiteRequest xsr = new CrossSiteRequest(fMsg.getUserMsg(), fMsg.getSenderId());
							_crossSiteRequests.addElement(xsr);
							_xsrHandler.handleCrossSiteRequest(fMsg.getUserMsg(), fMsg.getSenderId());
						}
					}
				});

		subscribeToMessageType(MessageType.TopologyChange, FerariConstants.TOPOLOGY_CHANGE_TOPIC, new MsgLsnr() {
			@Override
			public void onMessage(FerariMessage fMsg) {
				_logger.debug("Got message " + fMsg + " on topic " + FerariConstants.TOPOLOGY_CHANGE_TOPIC);
				if (_topReqHandler != null) {
					_logger.debug("Topology changed, invoking topology change listener");
					Serializable s = fMsg.getUserMsg();
					Map<String, List<String>> topGraph = ((TopologyPlan.TopologyConfig)s).getTopologyGraph();
					Map<String, Serializable> customConfig = ((TopologyPlan.TopologyConfig)s).getCustomConfig();
					_topReqHandler.onTopologyChange(topGraph, customConfig);
				} else {
					_logger.debug("_topReqHandler is null");
				}
				if (_xsrHandler != null) {
					_logger.debug("Topology changed, cleaning up all cross site request handlers");
					Iterator<CrossSiteRequest> i = _crossSiteRequests.iterator();
					while (i.hasNext()) {
						CrossSiteRequest xsr = i.next();
						_xsrHandler.cleanCrossSiteRequest(xsr.msg, xsr.source);
						i.remove();
					}
				} else {
					_logger.debug("_xsrHandler is null");
				}
			}
		});
	}

	private void subscribeToMessageType(MessageType msgType, String topic, MsgLsnr lsnr) throws JMSException {
		Topic top = _session.getSess().createTopic(topic);
		MessageConsumer consumer = _session.getSess().createConsumer(top);
		consumer.setMessageListener(new MessageListener() {
			@Override
			public void onMessage(Message message) {
				FerariMessage fMsg = ClientUtils.extractMessage(message, _logger);
				if (fMsg.getMsgType() != msgType) {
					_logger.warn("Got message of type " + fMsg.getMsgType() + " but expected " + msgType);
					return;
				}
				_logger.debug("Got message:" + fMsg);
				lsnr.onMessage(fMsg);
			}
		});
	}

	@Override
	public void setCrossSiteRequestHandler(CrossSiteRequestHandler handler) {
		this._xsrHandler = handler;
	}

	@Override
	public void close() {
		_session.close();
	}

	private static class CrossSiteRequest {
		private Serializable msg;
		private String source;

		private CrossSiteRequest(Serializable msg, String source) {
			this.msg = msg;
			this.source = source;
		}

		public boolean equals(Object o) {
			if (o instanceof CrossSiteRequest) {
				CrossSiteRequest xsr = (CrossSiteRequest) o;
				return msg.equals(xsr.msg) && source.equals(xsr.source);
			}
			return false;
		}

		public String toString() {
			return "from " + source + ":[" + msg + "]";
		}
	}

	@Override
	public void notifyTopologyChange(Map<String, List<String>> topologyGraph, Map<String, Serializable> customConfig) {
		try {
			TopologyConfig topconfig = new TopologyConfig(topologyGraph, customConfig);
			broadcastMessage(topconfig, FerariMessage.MessageType.TopologyChange, FerariConstants.TOPOLOGY_CHANGE_TOPIC);
			_logger.debug("Sending " + topconfig);
		} catch (JMSException e) {
			_logger.error("Failed sending topology change", e);
		}
	}

	private void broadcastMessage(Serializable msg, FerariMessage.MessageType msgType, String topic)
			throws JMSException {
		if (_session == null) {
			_logger.error("_session is null for " +  _siteId + ", aborting broadcast");
			return;
		}
		Topic top = _session.getSess().createTopic(topic);
		MessageProducer topicSender = _session.getSess().createProducer(top);
		FerariMessage ferrMsg = new FerariMessage("", "", msg);
		ferrMsg.setMsgType(msgType);
		ObjectMessage objMsg = _session.getSess().createObjectMessage();
		objMsg.setObject(ferrMsg);
		topicSender.send(objMsg);
		topicSender.close();
	}

	@Override
	public void setOptimizerRequestHandler(MessageHandler handler) {
		_optReqHandler = handler;
	}

	@Override
	public void broadcastInOurSite(Serializable msg) {
		try {
			broadcastMessage(msg, FerariMessage.MessageType.IntraSiteRequest, FerariConstants.INTRASITE_BROADCAST);
		} catch (JMSException e) {
			_logger.error("Failed broadcasting", e);
		}
	}

	void forwardMessageFromOptimizer(FerariMessage msg) {
		try {
			broadcastMessage(msg, FerariMessage.MessageType.OptimizerRequest, FerariConstants.OPTIMIZER_REQUEST_TOPIC);
		} catch (JMSException e) {
			_logger.error("Failed forwarding message from optimizer", e);
		}
	}

	@Override
	public void setbroadcastRequestHandler(MessageHandler handler) {
		_broadcastReqHandler = handler;
	}

	private static interface MsgLsnr {
		public void onMessage(FerariMessage fMsg);
	}

	@Override
	public void setTopologyRequestHandler(TopologyRequestHandler reqHandler) {
		_topReqHandler = reqHandler;
	}

}
