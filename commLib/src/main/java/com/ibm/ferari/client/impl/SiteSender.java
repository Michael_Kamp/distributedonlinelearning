package com.ibm.ferari.client.impl;

import java.io.IOException;
import java.net.Socket;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.Semaphore;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import com.ibm.ferari.FerariConstants;
import com.ibm.ferari.FerariMessage;
import com.ibm.ferari.exceptions.FerariStartupException;
import com.ibm.ferari.util.Util;

class SiteSender implements Runnable, MessageListener {

	private String[] _brokers;
	private volatile boolean _isConnected = false;
	private MessageProducer _dataProducer;
	// private MessageProducer _ctrlProducer;
	private volatile Session _session;
	private volatile Connection _connection;
	protected com.ibm.ferari.util.Logger _logger;
	private int _maxRetransTimeThresh;
	private LocalMessageStorage _msgStore;
	private volatile boolean _alive = true;
	private String _id;
	private volatile boolean _isFindingMaster = false;
	private List<ConnectionListener> _connLsnrs = new Vector<ConnectionListener>();
	private Thread _connThread;

	SiteSender(String id, String targetSite, String[] brokers, int maxRetransTimeThresh, com.ibm.ferari.util.Logger logger)
			throws FerariStartupException {
		_maxRetransTimeThresh = maxRetransTimeThresh;
		_id = id;
		_brokers = brokers;
		_logger = logger;

		try {
			_msgStore = new LocalMessageStorage(targetSite);
		} catch (IOException e) {
			throw new FerariStartupException("Failed initializing local message store", e);
		}

		while (!_isConnected) {
			findMaster();
			Util.sleep(1);
		}
		//System.err.println(id + " found master! (" + targetSite + ")");
		retransmitLostMessages();
		_connThread = new Thread(this, "FeariSiteClient[" + id + "-->" + targetSite + "]");
		_connThread.start();
	}

	private void retransmitLostMessages() {
		_logger.info("Recovering unsent messages...");
		int c = 0;
		FerariMessage message;
		try {
			_msgStore.prepareRecovery();
			while ((message = _msgStore.getNextMessage()) != null) {
				c++;
				// if message is too old, skip sending it
				long delta = System.currentTimeMillis() - message.getTimestamp();
				if (delta / 1000 > _maxRetransTimeThresh) {
					_logger.info("Message too old, discarding it");
					continue;
				}
				sendMsg(message);
			}
			_logger.info("Recovered " + c + " messages");
		} catch (Exception e) {
			_logger.error("Failed retransmitting lost messages", e);
		}
	}

	private void initSession(String host, int port) throws JMSException {
		waitForConnectionAccept(host, port);
		_logger.info("Entering, Initiating session to " + host + ":" + port);
		// Create a ConnectionFactory
/*		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				"failover:(tcp://" + host + ":" + port + ")?maxReconnectAttempts=0");*/
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://" + host + ":" + port);
		connectionFactory.setClientID(_id + "." + UUID.randomUUID());
		connectionFactory.setConnectionIDPrefix("FerariClient");

		// Create a Connection
		ActiveMQConnection connection = (ActiveMQConnection) connectionFactory.createConnection();

		connection.start();

		_logger.debug("Started connection");
		
		// Create a Session
		Session sess = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		_logger.debug("Created session");

		_connection = connection;
		_session = sess;
		
		// Create the destination (Topic or Queue)
		Topic to_coord_topic = _session.createTopic(FerariConstants.TO_COORDINATOR_DATA_TOPIC_NAME);
		//Destination to_coord_dest = _session.createQueue(FerariConstants.TO_COORDINATOR_DATA_TOPIC_NAME + "." + _id);
		_dataProducer = _session.createProducer(to_coord_topic);

		_dataProducer.setDeliveryMode(DeliveryMode.PERSISTENT);

		MessageConsumer queueConsumer = _session
				.createConsumer(_session.createTopic(FerariConstants.FROM_COORDINATOR_DATA_TOPIC_NAME));

		queueConsumer.setMessageListener(this);

		// _ctrlProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

		_connection.setExceptionListener(new ExceptionListener() {
			@Override
			public void onException(JMSException jmsException) {
				try {
					_logger.error("Disconnected from master!", jmsException);
					_isConnected = false;
					fireConnLsnrs(false);
					synchronized (SiteSender.this) {
						if (_isFindingMaster) {
							return;
						}
						if (_session != null) {
							_session.close();
						}
						_connection.close();
					}
				} catch (JMSException e) {
					_logger.error("Failed terminating session", e);
				} finally {
					try {
						_logger.debug("Stopping connection executors");
						// ((ActiveMQConnection)_connection).getTransport().stop();
						// ((ActiveMQConnection)_connection).getSessionTaskRunner().getExecutor().shutdown();
						((ActiveMQConnection) _connection).getSessionTaskRunner().shutdown();
					} catch (Exception e) {
						_logger.error("Failed shutting down transport", e);
					}
				}
			}
		});
		_logger.debug("Exiting");
	}

	public void addConnectionListener(ConnectionListener lsnr) {
		_connLsnrs.add(lsnr);
	}

	private void fireConnLsnrs(boolean connected) {
		_logger.debug("Entering(connected=" + connected + "), _connLsnrs size is " + _connLsnrs.size());
		Iterator<ConnectionListener> i = _connLsnrs.iterator();
		while (i.hasNext()) {
			i.next().onStatusChange(connected);
		}
		_logger.debug("Exiting");
	}

	public void onMessage(Message msg) {
		FerariMessage fMsg = ClientUtils.extractMessage(msg, _logger);
		if (fMsg != null) {
			handleMsg(fMsg);
		}
	}

	protected void handleMsg(FerariMessage msg) {
		_logger.warn("Received message of type " + msg.getMsgType().name() + " but don't know what to do with it");
	}

	private synchronized void findMaster() {
		_logger.info(_id + ":Finding active broker");
		// If we've been connected by a previous findMaster invocation
		if (_isConnected) {
			_logger.info(_id + ":Already connected, aborting");
			return;
		}

		Semaphore connectorThreadsSem = new Semaphore(0);
		for (String candidate : _brokers) {
			final String host = candidate.split(":")[0];
			final int port = Integer.parseInt(candidate.split(":")[1]);
			_logger.info(_id + ":Probing " + host + ":" + port);
			Util.runAsyncTask(new Runnable() {
				@Override
				public void run() {
					try {
						initSession(host, port);
						_isConnected = true;
						_logger.info(_id + ":Found broker: " + host + ":" + port);
						connectorThreadsSem.release(_brokers.length);
					} catch (JMSException e) {
						_logger.error(_id + ":Failed connecting to " + host + ":" + port);
						connectorThreadsSem.release();
					}
				}
			});
		} // for

		try {
			connectorThreadsSem.acquire(_brokers.length);
		} catch (InterruptedException e) {
			_logger.warn(_id + ":Got interrupt while waiting for connectorThreadsSem");
		}

		if (!_isConnected) {
			_logger.error(_id + ":Couldn't find an available broker");
		} else {
			_logger.info(_id + ":Connected to broker");
			fireConnLsnrs(true);
		}
	}

	public synchronized void sendMsg(FerariMessage ferraryMessage) {
		//System.out.println("sendMsg(" + message + "," + recipient + ")");
		if (_isConnected && (!_isFindingMaster)) {
			try {
				ObjectMessage msg = _session.createObjectMessage();
				//FerariMessage ferraryMessage = new FerariMessage(_id, recipient, message);
				msg.setObject(ferraryMessage);
				_logger.debug(_id + ":Sending message " + ferraryMessage);
				_dataProducer.send(msg);
				//System.err.println(_id + " sent message: " + ferraryMessage);
				return;
			} catch (JMSException e) {
				_isConnected = false;
				fireConnLsnrs(false);
				_logger.error(_id + ":Failed sending message, switching to local storage", e);
			}
		}

		_logger.info(_id + ":Not connected to broker(_isConnected = " + _isConnected + "), logging to disk instead");

		try {
			//FerariMessage fm = new FerariMessage(_id, recipient, message);
			//System.err.println("--------------------------------------");
			//System.err.println("Logging " + fm + " to disk");
			//System.err.println("--------------------------------------");
			_msgStore.appendMessage(ferraryMessage.toByteArray());
		} catch (IOException e) {
			_logger.error("Failed appending message", e);
		}
	}

	boolean isRunning() {
		return _connThread.isAlive();
	}

	public synchronized void close() {
		_logger.info("Closing client");
		try {
			_alive = false;
			_connThread.interrupt();

			// _ctrlProducer.close();
			if (_dataProducer != null) {
				_dataProducer.close();
			}
			if (_session != null) {
				_session.close();
			}

			if (_connection != null) {
				_connection.setExceptionListener(null);
				_connection.stop();
				_connection.close();
			}
		} catch (JMSException e) {
			_logger.error("Failed closing connection", e);
		}
	}

	@Override
	public void run() {
		while (_alive) {
			Util.sleep(1);
			if (!_alive) {
				return;
			}
			// Already connected, abort finding master
			if (_isConnected) {
				continue;
			}

			_isFindingMaster = true;
			findMaster();

			if (!_alive) {
				return;
			}
			synchronized (this) {
				_isFindingMaster = false;
				_logger.info("Reconnecting to coordinator");
				retransmitLostMessages();
			}
		}
	}
	
	private static void waitForConnectionAccept(String host, int port) {
		while (true) {
			try {
				Socket s = new Socket(host, port);
				s.close();
				return;
			} catch (IOException e) {
			}
		}

	}

}
