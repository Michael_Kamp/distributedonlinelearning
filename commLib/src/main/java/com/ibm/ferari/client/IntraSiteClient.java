package com.ibm.ferari.client;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.ibm.ferari.client.impl.CrossSiteRequestHandler;

public interface IntraSiteClient {
	
	/***
	 * Sets the {@link CrossSiteRequestHandler} to be invoked when some {@link InterSiteClient} sends a cross site request
	 * @param handler
	 */
	public void setCrossSiteRequestHandler(CrossSiteRequestHandler handler);
	
	/***
	 * Shuts down the client
	 */
	public void close();

	/***
	 * Method used to notify the {@link IntraSiteClient} of a topology change, 
	 * don't use it if you don't know the consequences
	 */
	public void notifyTopologyChange(Map<String, List<String>> topologyGraph, Map<String, Serializable> customConfig);
	
	/***
	 * Sets a message handler that processes messages sent by the optimizer 
	 * @param handler
	 */
	public void setOptimizerRequestHandler(MessageHandler handler);
	
	/****
	 * Sets a message handler for messages that are broadcasted within the site
	 * @param handler
	 */
	public void setbroadcastRequestHandler(MessageHandler handler);
	
	/****
	 * Broadcasts a message to all {@link IntraSiteClient}s in our site
	 * @param msg
	 */
	public void broadcastInOurSite(Serializable msg);
	
	public void setTopologyRequestHandler(TopologyRequestHandler reqHandler);
	

}
