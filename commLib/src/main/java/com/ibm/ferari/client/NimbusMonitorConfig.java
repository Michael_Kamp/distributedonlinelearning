package com.ibm.ferari.client;

import com.ibm.ferari.FerariConstants;
import com.ibm.ferari.StormMonitoringFilter;

public class NimbusMonitorConfig {
	
	public void validate() {
		for (Object o : new Object[]{monitoringFilter, nimbusHost}) {
			if (o == null) {
				throw new RuntimeException("NimbusMonitorConfig is not properly configured, missing some parameters");
			}
		}
	}
	
	public StormMonitoringFilter getMonitoringFilter() {
		return monitoringFilter;
	}
	public void setMonitoringFilter(StormMonitoringFilter monitoringFilter) {
		this.monitoringFilter = monitoringFilter;
	}
	public int getMonitorInterval() {
		return monitorInterval;
	}
	public void setMonitorInterval(int monitorInterval) {
		this.monitorInterval = monitorInterval;
	}
	public String getNimbusHost() {
		return nimbusHost;
	}
	public void setNimbusHost(String nimbusHost) {
		this.nimbusHost = nimbusHost;
	}
	public int getNimbusPort() {
		return nimbusPort;
	}
	public void setNimbusPort(int nimbusPort) {
		this.nimbusPort = nimbusPort;
	}
	private StormMonitoringFilter monitoringFilter;
	private int monitorInterval = FerariConstants.NIMBUS_MONITOR_INTERVAL;
	private String nimbusHost = "localhost";
	private int nimbusPort = FerariConstants.NIMBUS_DEF_PORT;
	
}
