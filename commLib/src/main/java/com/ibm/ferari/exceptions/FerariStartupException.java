package com.ibm.ferari.exceptions;

public class FerariStartupException extends RuntimeException {
	
	private static final long serialVersionUID = 6939999780128918269L;

	public FerariStartupException(String msg,Throwable e) {
		super(msg,e);
	}
	
	public FerariStartupException(String msg) {
		super(msg);
	}
	

}
