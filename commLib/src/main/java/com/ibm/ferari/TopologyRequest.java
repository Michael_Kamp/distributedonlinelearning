package com.ibm.ferari;

import java.io.IOException;

public class TopologyRequest extends FerariMessage {
	
	private static final long serialVersionUID = -8525528489467422664L;

	public TopologyRequest(String senderId) throws IOException {
		super(senderId, null,null);
		setMsgType(MessageType.TopologyRequest);
	}

}
