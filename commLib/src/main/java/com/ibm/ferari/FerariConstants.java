package com.ibm.ferari;

public interface FerariConstants {
	
	public static final String TO_COORDINATOR_DATA_TOPIC_NAME = "TO_COORD_DATA";
	public static final String FROM_COORDINATOR_DATA_TOPIC_NAME = "FROM_COORD_DATA";
	public static final String TO_COORDINATOR_CONTROL_TOPIC_NAME = "TO_COORD_CONTROL";
	public static final String FROM_COORDINATOR_CONTROL_TOPIC_NAME = "FROM_COORD_CONTROL";
	public static final String INTRA_BROKER_TOPIC_NAME = "INTRA_BROKER";
	public static final String NIMBUS_THRIFT_DEF_PROTOCOL = "backtype.storm.security.auth.SimpleTransportPlugin";
	public static final String COORD_CLIENT_ID = "Coordinator";	
	public static final String FERRARY_MODE_ATTR = "ferrary.mode";
	public static final String FERRARY_MODE_TEST_VAL = "test";
	public static final String FERARY_CONSOLE_LOG_ATTR = "ferrari.console.log";
	public static final String FERARY_DEBUG_ATTR = "ferrari.log.debug";
	public static final String OPTIMIZER = "OPTIMIZER";
	
	
	public static final int RCAST_WORKER_NUM = 10;
	public static final int MAX_QUEUE_IDLE_TIME = 60;
	public static final int NIMBUS_DEF_PORT = 6627;
	public static final int NIMBUS_MONITOR_INTERVAL = 10;
	public static final int BROKER_WATCHER_STARTUP_WAIT_TIME = 10;
	public static final int BROKER_WATCHER_MAX_DISCONNECT_TIME = 10;
	public static final int BROKER_WATCHER_SLEEPTIME = 1;
	public static final int RECOVER_EXIT_CODE = 5;
	public static final int NO_MASTER_PANIC_THRESH = 3;
	public static final String ANONYMOUS_CLIENT_ID = "ANONYMOUS";
	public static final String CROSS_SITE_REQUEST_TOPIC = "CROSS_SITE_REQUEST";
	public static final String INTRASITE_BROADCAST = "INTRASITE_BROADCAST";
	public static final String OPTIMIZER_REQUEST_TOPIC = "OPT_REQUEST";
	public static final String TOPOLOGY_CHANGE_TOPIC = "TOP_CHANGE";
	public static final long STORM_TOP_STARTUP_TIMELIMIT_MILLIS = 20 * 1000;
	String MEMBERSHIP_TOPIC_NAME = "SITES";
	

	

}
