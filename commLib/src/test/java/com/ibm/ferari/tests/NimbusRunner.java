package com.ibm.ferari.tests;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class NimbusRunner {

	public static void main(String[] args) {
		try {
			System.setOut(new PrintStream("nimbus.out"));
			System.setErr(new PrintStream("nimbus.err"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		backtype.storm.daemon.nimbus.main(new String[]{});
	}

}
